﻿using PixelArt.Data.Models;
using PixelArt.Utils;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UniRx.Async;
using UnityEngine;

namespace PixelArt.Data
{
    public class AppStore : Singleton<AppStore>
    {
        public ProgressData UserProgress { get; private set; }
        public ImageInfo CurrentImageInfo { get; private set; }

        List<string> _defaultAssetImageNames = new List<string>();

        public static async UniTask Init()
        {
            #region load user progress
            var progressDataFileName = FileUtils.GetFullFileName(FileUtils.PROGRESS_DATA_FILE_NAME);

            if (!FileUtils.FileExistCheck(progressDataFileName))
            {
                Instance.UserProgress = new ProgressData { dropletAmount = 250 };

                await Instance.SaveProgressData();
            }

            var progressData = await FileUtils.LoadFromJson<ProgressData>(progressDataFileName);

            Instance.UserProgress = progressData;
            #endregion

            #region load image urls
            var defaultImageNames = await FileUtils.LoadDefaultAssetImageNames();

            Instance._defaultAssetImageNames = defaultImageNames;
            #endregion
        }

        public void SetCurrentImageInfo(ImageInfo imageInfo) => CurrentImageInfo = imageInfo;


        public static async UniTask LoadCurrentImageData()
        {
            if (Instance.CurrentImageInfo == null) return;

            // определяем наличие метаданных по выбранному изображению
            var currentImage = Instance.CurrentImageInfo;
            var metaFileName = FileUtils.GetMetaFileName(currentImage.fileName);

            if (new FileInfo(metaFileName).Exists)
            {
                // при наличии метаданных читаем из метафайла и записываем в ImageInfo.imageData
                var data = await FileUtils.LoadFromJson<ImageData>(metaFileName);

                data.InitAfterLoad();

                currentImage.SetImageData(data);
            }
            else
            {
                if (currentImage.originalImage == null)
                {
                    // при отсутствии изображения загружаем из ресурсов приложения
                    var imageFile = await FileUtils.LoadImageFile(currentImage.originalName, currentImage.imageType == ImageType.Default);

                    currentImage.SetImageData(imageFile);
                }

                // сохраняем метаданные в файл
                await FileUtils.SaveToJson(metaFileName, currentImage.imageData);
                await Instance.SaveProgressData();
            }
        }

        public static async UniTask SaveCurrentImageData()
        {
            // сохраняем общий прогресс
            await Instance.SaveProgressData();

            var currentImage = Instance.CurrentImageInfo;

            if (currentImage == null) return;

            var previewImageName = FileUtils.GetFullFileName(currentImage.fileName);
            var metaFileName = FileUtils.GetMetaFileName(currentImage.fileName);

            // сохраняем прогресс рисунка в метафайле
            await FileUtils.SaveToJson(metaFileName, currentImage.imageData);

            // генерируем и сохраняем previewImage для этой картинки
            var previewImage = currentImage.imageData.GeneratePreviewImage();

            await FileUtils.SaveImageFile(previewImageName, previewImage);
        }

        public static async UniTask LoadOriginalImage()
        {
            var currentImage = Instance.CurrentImageInfo;

            var imageFile = await FileUtils.LoadImageFile(currentImage.originalName, false);

            Instance.CurrentImageInfo.originalImage = imageFile;
        }

        public static async UniTask LoadPreviewImages(List<ImageInfo> images)
        {
            foreach (var image in images)
            {
                var fullName = FileUtils.GetFullFileName(image.fileName);

                var preview = await FileUtils.LoadImageFile(fullName, remoteLoad: false);

                image.SetPreviewImage(preview);
            }
        }


        public static bool FindRandomImageAndCreateImageInfo()
        {
            var imageName = Instance.GetRandomNotOpenedImageName();

            if (string.IsNullOrEmpty(imageName)) return false;

            var imageInfo = ImageInfo.CreteNew(imageName, ImageType.Default);

            Instance.AddImageInfoToProgress(imageInfo);

            return true;
        }


        public async UniTask SaveProgressData()
        {
            var progressDataFileName = FileUtils.GetFullFileName(FileUtils.PROGRESS_DATA_FILE_NAME);
            await FileUtils.SaveToJson(progressDataFileName, UserProgress);
        }

        public static async UniTask ResetCurrentImageProgress()
        {
            Instance.CurrentImageInfo.ResetImageData();

            await SaveCurrentImageData();
        }


        public void AddImageInfoToProgress(ImageInfo imageInfo)
        {
            UserProgress.imageItems.Add(imageInfo);

            SetCurrentImageInfo(imageInfo);
        }

        public void RemoveImageInfoFromProgress(ImageInfo imageInfo)
        {
            UserProgress.imageItems.Remove(imageInfo);

            SetCurrentImageInfo(null);
        }


        public static async UniTask DeleteImageFile(ImageInfo imageInfo)
        {
            Instance.RemoveImageInfoFromProgress(imageInfo);

            await Instance.SaveProgressData();

            var imageFile = FileUtils.GetFullFileName(imageInfo.fileName);
            var metaFile = FileUtils.GetMetaFileName(imageInfo.fileName);

            FileUtils.RemoveFile(imageFile);
            FileUtils.RemoveFile(metaFile);
        }

        public int GetCountNotOpenedImage()
        {
            var openedImage = UserProgress.imageItems.Select(x => x.originalName);
            var notOpenedImages = _defaultAssetImageNames.Except(openedImage);

            return notOpenedImages.Count();
        }

        public string GetRandomNotOpenedImageName()
        {
            var openedImage = UserProgress.imageItems.Select(x => x.originalName);
            var notOpenedImage = _defaultAssetImageNames.Except(openedImage).ToArray();

            if (notOpenedImage.Length == 0) return null;

            var imageName = notOpenedImage[Random.Range(0, notOpenedImage.Length)];

            Debug.Log($"random name: {imageName}");

            return imageName;
        }
    }
}