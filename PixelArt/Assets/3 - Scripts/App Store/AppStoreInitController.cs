﻿using PixelArt.Data;
using PixelArt.SceneManagement;
using UniRx.Async;
using UnityEngine;

public class AppStoreInitController : MonoBehaviour
{
    void Start() => UniTask.ToCoroutine(async () =>
    {
        await AppStore.Init();

        SceneController.Instance.LoadMenuScene();
    });
}
