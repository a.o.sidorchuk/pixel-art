﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace PixelArt.Data.Models
{
    public enum PriorityColor
    {
        Main,
        Self
    }

    public enum BrushType
    {
        Pixel,
        FillOfArea,
        FillOfCircle,
        FillOfColor
    }

    public enum PlayerInput
    {
        Tap,
        Stick,
        TapAndMove
    }

    public enum ImpactToAnotherPixel
    {
        Ignore,
        UseMainColor,
        UseSelfColor
    }

    public class BrushSaveParam
    {
        public int id;
        public bool isPurchased;
        public int storageValue;
        public bool isReloading;
        public int reloadValue;
    }

    [CreateAssetMenu(menuName = "PixelArt/Brush")]
    public class BrushData : ScriptableObject
    {
        [HideInInspector] public int id = -1;

        [Tooltip("Название кисти")]
        public string title;
        [Tooltip("Ссылка на иконку")]
        public AssetReference icon;

        [Tooltip("Кисть куплена")]
        public bool isPurchased;

        [Space]
        [Tooltip("Приоритетный цвет при закраске пикселя")]
        public PriorityColor priorityColor;

        [Tooltip("Тип кисти")]
        public BrushType type;

        [Tooltip("Тип пользовательского ввода")]
        public PlayerInput playerInput;

        [Tooltip("Тип воздействия на пиксель с другим цветом")]
        public ImpactToAnotherPixel impactToAnotherPixel;

        [Space]
        [Tooltip("Счетчик перезарядки")]
        public int reloadCount;
        /// <summary> Текущий прогресс перезарядки </summary>
        public int currentReloadCount { get; private set; }
        /// <summary> Выполняется перезарядка </summary>
        public bool isReloading { get; private set; }

        [Space]
        [Tooltip("Стоимость использования кисти:\n\"+2\" - начислить, \"-2\" - списать")]
        public int cost;

        [Space]
        [Tooltip("Радиус закрашивающего круга")]
        public float circleRadius;

        [Space]
        [Tooltip("Использование хранилища краски")]
        public bool paintStorageOn;
        [Tooltip("Размер хранилиша краски")]
        public int paintStorageAmount;
        /// <summary> Остаток краски в хранилище </summary>
        public int remainsInPaintStorage { get; private set; }

        [Space]
        [Tooltip("Штраф за закраску не тем цветом")]
        public bool fineOn;
        [Tooltip("Стоимость штрафа при закраске не тем цветом")]
        public int costOfFine;

        #region Storage
        public float GetStoragePercent()
        {
            if (!paintStorageOn) return 0;
            return remainsInPaintStorage * 1.0f / paintStorageAmount;
        }

        public void RemainsInPaintStorageDecrement(int count)
        {
            remainsInPaintStorage = Mathf.Clamp(remainsInPaintStorage - count, 0, paintStorageAmount);
        }
        #endregion

        #region Reload
        public void StartReload()
        {
            if (isReloading || reloadCount == 0) return;
            isReloading = true;
        }

        public void StopReload()
        {
            if (!isReloading || reloadCount == 0) return;
            isReloading = false;
            currentReloadCount = 0;
        }

        public float GetReloadPercent()
        {
            if (!isReloading) return 0;
            return 1f - (currentReloadCount * 1.0f / reloadCount);
        }

        public void ReloadCountIncrement()
        {
            if (!isReloading || reloadCount == 0) return;
            currentReloadCount = Mathf.Clamp(currentReloadCount + 1, 0, reloadCount);
            if (currentReloadCount >= reloadCount)
            {
                StopReload();
            }
        }
        #endregion

        #region Purchase
        /// <summary> Покупка кисти </summary>
        /// <param name="state">true - куплено, false - израсходовано</param>
        public void SetPurchasedState(bool state)
        {
            isPurchased = cost < 0 ? state : true;

            if (paintStorageOn && state)
            {
                remainsInPaintStorage = paintStorageAmount;
            }
        }
        #endregion

        #region Save
        public BrushSaveParam GetSaveParams()
        {
            return new BrushSaveParam
            {
                id = id,
                isPurchased = isPurchased,
                storageValue = remainsInPaintStorage,
                isReloading = isReloading,
                reloadValue = currentReloadCount
            };
        }

        public void SetParams(BrushSaveParam param)
        {
            isPurchased = param.isPurchased;
            remainsInPaintStorage = param.storageValue;
            isReloading = param.isReloading;
            currentReloadCount = param.reloadValue;
        }
        #endregion

        private void OnEnable()
        {
#if UNITY_EDITOR 
            // автоназначение Id при создании кисти
            if (id != -1) return;

            var maxId = 0;

            var brushAssets = UnityEditor.AssetDatabase.FindAssets("t:BrushData");
            foreach (var brushGUID in brushAssets)
            {
                var brush = UnityEditor.AssetDatabase.LoadAssetAtPath<BrushData>(UnityEditor.AssetDatabase.GUIDToAssetPath(brushGUID));
                if (brush.id > maxId) maxId = brush.id;
            }

            id = ++maxId;
#endif
        }
    }
}
