﻿using Newtonsoft.Json;
using PixelArt.Utils.Image;
using System.Collections.Generic;
using UnityEngine;

namespace PixelArt.Data.Models
{
    #region CustomVector2Int
    public struct CustomVector2Int
    {
        public int x, y;
        public CustomVector2Int(int x, int y) { this.x = x; this.y = y; }
    }

    public static class CustomVector2IntExtensions
    {
        public static Vector3Int GetVector3Int(this CustomVector2Int custom) => new Vector3Int(custom.x, custom.y, 0);
        public static CustomVector2Int GetCustomVector3Int(this Vector3Int vector) => new CustomVector2Int(vector.x, vector.y);
    }
    #endregion

    #region Palette
    public class PaletteItem
    {
        public int id;

        public Color32 color;
        public bool isWhiteColor;       // это цвет из белого диапазона и его не нужно закрашивать

        public int pixelCount;
        public int coloredPixelCount;

        public void ColoredPixelIncrement() => coloredPixelCount = Mathf.Clamp(coloredPixelCount + 1, 0, pixelCount);

        public float GetProgress() => coloredPixelCount * 1f / pixelCount;

        public void ResetProgress() => coloredPixelCount = 0;
    }
    #endregion

    #region Pixel
    public class PixelItem
    {
        [JsonIgnore] public PaletteItem paletteItem;
        [JsonIgnore] public Vector3Int position;

        public Color32 wrongСolor;

        public int pickId;
        public bool? isRealColor;   // закрашен ли пиксель? правильным цветом?
        public bool hiddenOfFog = true;    // над этим пикселем есть "туман"

        #region json save props
        private int _paletteId;
        [JsonProperty] private int paletteId { get => paletteItem.id; set => _paletteId = value; }
        [JsonProperty] private CustomVector2Int pos { get => position.GetCustomVector3Int(); set => position = value.GetVector3Int(); }
        #endregion

        public PixelItem() { }
        public PixelItem(PaletteItem paletteItem, Vector3Int position)
        {
            this.paletteItem = paletteItem;
            this.position = position;
        }

        public void Init(List<PaletteItem> palette) => paletteItem = palette[_paletteId];

        public Color32 GetColor() => (isRealColor.HasValue && isRealColor.Value) ? paletteItem.color : new Color32(255, 255, 255, 255);
        public Color32 GetCurrentColor() => isRealColor.Value ? paletteItem.color : wrongСolor;

        public void SetColor(PaletteItem paletteItem, int pickId, out bool? oldRealColorState)
        {
            this.pickId = pickId;

            oldRealColorState = isRealColor;
            isRealColor = paletteItem == null || this.paletteItem == paletteItem;

            if (!isRealColor.Value)
            {
                wrongСolor = paletteItem.color;
                wrongСolor.a /= 2;
            }
            else
                this.paletteItem.ColoredPixelIncrement();
        }

        public void ResetProgress()
        {
            pickId = 0;
            isRealColor = null;
            hiddenOfFog = true;
        }
    }
    #endregion

    public class ImageData
    {
        [JsonProperty] public int width { get; private set; }
        [JsonProperty] public int height { get; private set; }
        [JsonProperty] public List<PaletteItem> palette { get; private set; }
        [JsonProperty] public List<PixelItem> pixels { get; private set; }

        [JsonProperty] public int pixelCount { get; private set; }
        [JsonProperty] public int coloredPixelCount { get; private set; }
        [JsonProperty] public int errorPixelCount { get; private set; }

        public ImageData() { }
        public ImageData(int width, int height, List<PaletteItem> palette, List<PixelItem> pixels, int pixelCount)
        {
            this.width = width;
            this.height = height;
            this.palette = palette;
            this.pixels = pixels;
            this.pixelCount = pixelCount;
        }

        public void InitAfterLoad()
        {
            // обновить ссылки на пилитру после загрузки из json
            foreach (var item in pixels) item.Init(palette);
        }

        public PixelItem GetPixel(int x, int y) => pixels[ImageUtils.CalcIndex(x, y, width)];
        public PixelItem GetPixel(Vector2Int position) => GetPixel(position.x, position.y);

        public float GetProgressPercent() => coloredPixelCount * 1.0f / pixelCount;
        public float GetErrorPercent() => errorPixelCount * 1.0f / pixelCount;

        public void SetColor(IEnumerable<PixelItem> pixels, PaletteItem palette = null)
        {
            foreach (var pixel in pixels) SetColor(pixel, palette);
        }
        public void SetColor(PixelItem pixel, PaletteItem paletteItem = null)
        {
            pixel.SetColor(paletteItem, coloredPixelCount, out bool? prevRealColorState);

            if (pixel.isRealColor.Value)
            {
                coloredPixelCount++;

                if (prevRealColorState.HasValue && !prevRealColorState.Value)
                    errorPixelCount--;
            }
            else if (!prevRealColorState.HasValue)
                errorPixelCount++;
        }

        public void ResetProgress()
        {
            coloredPixelCount = 0;
            errorPixelCount = 0;
            palette.ForEach(x => x.ResetProgress());
            pixels.ForEach(x => x.ResetProgress());
        }

        public bool CheckPosition(Vector2Int position)
        {
            return (position.x >= 0 && position.x < width) && (position.y >= 0 && position.y < height);
        }
    }
}