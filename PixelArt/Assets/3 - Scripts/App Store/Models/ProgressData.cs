﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.Data.Models
{
    public enum ImageType { Default, User }
    public enum ImageStatus { None, Started, Completed }

    /// <summary>Размер полотна в зависимости от сложности</summary>
    public enum ResizeType
    {
        VeryEasy = 32,
        Easy = 64,
        Medium = 128,
        Hard = 256,
        Hardcore = 300
    }

    /// <summary>Размер палитры в зависимости от сложности</summary>
    public enum PaletteSizeType
    {
        VeryEasy = 8,
        Easy = 16,
        Medium = 32,
        Hard = 64,
        Hardcore = 128
    }

    public class GameCameraParams
    {
        [JsonProperty] public float x { get; private set; }
        [JsonProperty] public float y { get; private set; }
        [JsonProperty] public float zoom { get; private set; }

        [JsonProperty] public RangeInt zoomRange { get; private set; }

        public void Init(Vector3 position, int minZoom)
        {
            SetPosition(position);
            SetZoomRange(minZoom);
            SetZoom(minZoom);
        }

        public void SetPosition(Vector3 position)
        {
            x = position.x;
            y = position.y;
        }

        public void SetZoomRange(int minZoom) => zoomRange = new RangeInt(8, minZoom - 8);

        public void SetZoom(float zoom) => this.zoom = zoom;

        public float GetZoomPercent() => 1 - (zoom / zoomRange.end);

        public Vector3 GetPosition() => new Vector3(x, y, -10);
    }

    public class ImageInfo
    {
        public string fileName;
        public string originalName;

        public ImageType imageType;
        public ImageStatus imageStatus;

        public ResizeType resizeType = ResizeType.Easy;
        public PaletteSizeType paletteSizeType = PaletteSizeType.Medium;

        public GameCameraParams gameCameraParams;

        [JsonIgnore] public ImageData imageData;

        [JsonIgnore] public Texture2D originalImage;
        [JsonIgnore] public Texture2D previewImage;

        public bool isCompleted => imageStatus == ImageStatus.Completed;

        public void ResetImageData()
        {
            imageStatus = ImageStatus.Started;
            imageData.ResetProgress();
        }

        public void SetImageData(ImageData imageData)
        {
            this.imageData = imageData;
        }

        public void SetImageData(Texture2D originalImage)
        {
            gameCameraParams = new GameCameraParams();
            gameCameraParams.Init(
                position: new Vector3 { x = originalImage.width / 2f, y = originalImage.height / 2f },
                minZoom: originalImage.width
            );

            this.originalImage = originalImage;
            imageData = originalImage.GenerateImageData((int)paletteSizeType);
        }

        public void SetImageData(Texture2D originalImage, Rect cropRect, ResizeType resizeType, PaletteSizeType paletteSizeType)
        {
            this.resizeType = resizeType;
            this.paletteSizeType = paletteSizeType;

            originalImage = originalImage.CropAndResize(cropRect, (int)resizeType);

            SetImageData(originalImage);
        }

        public void SetPreviewImage(Texture2D previewImage)
        {
            this.previewImage = previewImage;
        }


        public static ImageInfo CreteNew(string originalName, ImageType imageType)
        {
            var fileName = originalName.Split('/').Last();

            return new ImageInfo
            {
                fileName = fileName,
                originalName = originalName,
                imageStatus = ImageStatus.Started,
                imageType = imageType
            };
        }
    }

    public class ProgressData
    {
        /// <summary> Валюта: "Капельки" </summary>
        public int dropletAmount = 0;

        /// <summary> Сохраненные данные о кистях </summary>
        public List<BrushSaveParam> brushSaves = new List<BrushSaveParam>();

        /// <summary> Все открытые и добавленные картинки игроком </summary>
        public List<ImageInfo> imageItems = new List<ImageInfo>();

        public void AddDroplets(int amount)
        {
            dropletAmount = Mathf.Clamp(dropletAmount + amount, 0, int.MaxValue);
        }
        
        public void AddFine(BrushData brush)
        {
            dropletAmount = Mathf.Clamp(dropletAmount + brush.costOfFine, 0, int.MaxValue);
        }

        public bool BrushPurchasedProcess(BrushData brushData)
        {
            if (dropletAmount < Mathf.Abs(brushData.cost)) return false;

            dropletAmount += brushData.cost;

            if (dropletAmount < 0) dropletAmount = 0;

            brushData.SetPurchasedState(true);

            UpdateBrushSaveContainer(brushData);

            return true;
        }

        public void UpdateBrushSaveContainer(BrushData brushData)
        {
            var brushSaveParam = brushSaves.FirstOrDefault(x => x.id == brushData.id);

            if (brushSaveParam == null)
            {
                brushSaves.Add(brushData.GetSaveParams());
            }
            else
            {
                brushSaveParam.isPurchased = brushData.isPurchased;
                brushSaveParam.storageValue = brushData.remainsInPaintStorage;
                brushSaveParam.isReloading = brushData.isReloading;
                brushSaveParam.reloadValue = brushData.currentReloadCount;
            }
        }
    }
}