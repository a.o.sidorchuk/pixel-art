﻿using PixelArt.Data.Models;
using System.Collections.Generic;
using UnityEngine;

namespace PixelArt.Brushes.Abstractions
{
    public interface IBrush
    {
        List<PixelItem> GetPixels(Vector2Int point, PaletteItem palette, ImageData image);
    }
}

