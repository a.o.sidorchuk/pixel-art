﻿using PixelArt.Brushes.Abstractions;
using PixelArt.Data.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PixelArt.Brushes
{
    public class BrushFactory
    {
        static Dictionary<BrushType, Func<BrushData, IBrush>> _brushMapping = new Dictionary<BrushType, Func<BrushData, IBrush>>
        {
            { BrushType.Pixel, (settings) => new PixelBrush(settings) },
            { BrushType.FillOfArea, (settings) => new FillOfAreaBrush(settings) },
            { BrushType.FillOfCircle, (settings) => new FillOfCircleBrush(settings) },
            { BrushType.FillOfColor, (settings) => new FillOfColorBrush(settings) }
        };

        public static IBrush GetBrush(BrushData brushItem)
        {
            return _brushMapping[brushItem.type](brushItem);
        }
    }
}