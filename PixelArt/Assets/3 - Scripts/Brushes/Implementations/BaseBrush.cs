﻿using PixelArt.Brushes.Abstractions;
using PixelArt.Data.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.Brushes
{
    public abstract class BaseBrush : IBrush
    {
        protected readonly BrushData _brushItem;

        public BaseBrush(BrushData brushItem) => _brushItem = brushItem;

        public List<PixelItem> GetPixels(Vector2Int point, PaletteItem palette, ImageData image)
        {
            if (_brushItem.paintStorageOn && _brushItem.remainsInPaintStorage <= 0) return null;

            var pixels = GetPixelItems(point, palette, image);

            pixels = pixels
                .Where(x => !x.paletteItem.isWhiteColor &&
                    (!x.isRealColor.HasValue || x.isRealColor.HasValue && !x.isRealColor.Value))
                .ToList();

            var priorityPalette = _brushItem.priorityColor == PriorityColor.Main ? palette : null;

            switch (_brushItem.impactToAnotherPixel)
            {
                case ImpactToAnotherPixel.Ignore:
                    pixels = pixels.Where(x => x.paletteItem == palette).ToList();
                    image.SetColor(pixels, priorityPalette);
                    break;
                case ImpactToAnotherPixel.UseMainColor:
                    image.SetColor(pixels, priorityPalette);
                    break;
                case ImpactToAnotherPixel.UseSelfColor:
                    image.SetColor(pixels);
                    break;
            }

            // списание хранилища
            if (_brushItem.paintStorageOn)
            {
                pixels = pixels.Take(_brushItem.remainsInPaintStorage).ToList();
            }

            return pixels.ToList();
        }

        protected abstract List<PixelItem> GetPixelItems(Vector2Int point, PaletteItem palette, ImageData image);
    }
}