﻿using PixelArt.Brushes.Abstractions;
using PixelArt.Data.Models;
using PixelArt.Utils.Image;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PixelArt.Brushes
{
    public class FillOfAreaBrush : BaseBrush
    {
        #region params
        public struct FloodFillRange
        {
            public int startX;
            public int endX;
            public int y;

            public FloodFillRange(int startX, int endX, int y)
            {
                this.startX = startX;
                this.endX = endX;
                this.y = y;
            }
        }

        public class FloodFillRangeQueue
        {
            FloodFillRange[] array;
            int size;
            int head;

            public int Count => size;
            public FloodFillRange First => array[head];

            public FloodFillRangeQueue() : this(10000) { }

            public FloodFillRangeQueue(int initialSize)
            {
                array = new FloodFillRange[initialSize];
                head = 0;
                size = 0;
            }

            public void Enqueue(FloodFillRange r)
            {
                if (size + head == array.Length)
                {
                    var newArray = new FloodFillRange[2 * array.Length];
                    Array.Copy(array, head, newArray, 0, size);
                    array = newArray;
                    head = 0;
                }
                array[head + (size++)] = r;
            }

            public FloodFillRange Dequeue()
            {
                var range = new FloodFillRange();
                if (size > 0)
                {
                    range = array[head];
                    array[head] = new FloodFillRange();
                    head++;
                    size--;
                }
                return range;
            }
        }

        int width;
        int height;
        List<PixelItem> pixels;
        FloodFillRangeQueue ranges;
        int startPaletteId;
        bool[] pixelsChecked;
        #endregion

        public FillOfAreaBrush(BrushData brushItem) : base(brushItem) { }

        protected override List<PixelItem> GetPixelItems(Vector2Int point, PaletteItem palette, ImageData image)
        {
            var result = new List<PixelItem>();

            width = image.width;
            height = image.height;
            pixels = image.pixels;

            pixelsChecked = new bool[pixels.Count];

            ranges = new FloodFillRangeQueue(((width + height) / 2) * 5);

            var x = point.x;
            var y = point.y;
            var idx = ImageUtils.CalcIndex(x, y, width);
            startPaletteId = pixels[idx].paletteItem.id;

            LinearFill(x, y, result);

            while (ranges.Count > 0)
            {
                var range = ranges.Dequeue();

                var downPxIdx = (width * (range.y + 1)) + range.startX;
                var upPxIdx = (width * (range.y - 1)) + range.startX;

                var upY = range.y - 1;
                var downY = range.y + 1;

                for (var i = range.startX; i <= range.endX; i++)
                {
                    var tempIdx = ImageUtils.CalcIndex(i, upY, width);
                    if (range.y > 0 && !pixelsChecked[upPxIdx] && CheckPixel(tempIdx))
                        LinearFill(i, upY, result);

                    tempIdx = ImageUtils.CalcIndex(i, downY, width);
                    if (range.y < (height - 1) && !pixelsChecked[downPxIdx] && CheckPixel(tempIdx))
                        LinearFill(i, downY, result);

                    downPxIdx++;
                    upPxIdx++;
                }
            }

            return result;
        }

        private void LinearFill(int x, int y, List<PixelItem> result)
        {
            var lFillLoc = x;
            var pxIdx = ImageUtils.CalcIndex(x, y, width);

            result.Add(pixels[pxIdx]);
            pixelsChecked[pxIdx] = true;

            while (true)
            {
                lFillLoc--;
                pxIdx--;

                if (lFillLoc <= 0 || pixelsChecked[pxIdx] || !CheckPixel(pxIdx)) break;

                result.Add(pixels[pxIdx]);
                pixelsChecked[pxIdx] = true;
            }

            lFillLoc++;

            var rFillLoc = x;
            pxIdx = ImageUtils.CalcIndex(x, y, width);

            while (true)
            {
                rFillLoc++;
                pxIdx++;

                if (rFillLoc >= width || pixelsChecked[pxIdx] || !CheckPixel(pxIdx)) break;

                result.Add(pixels[pxIdx]);
                pixelsChecked[pxIdx] = true;
            }

            rFillLoc--;

            var r = new FloodFillRange(lFillLoc, rFillLoc, y);
            ranges.Enqueue(r);
        }

        private bool CheckPixel(int tempIdx)
        {
            return pixels[tempIdx].paletteItem.id == startPaletteId;
        }
    }
}