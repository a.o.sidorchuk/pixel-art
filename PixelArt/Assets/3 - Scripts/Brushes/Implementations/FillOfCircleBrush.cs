﻿using PixelArt.Brushes.Abstractions;
using PixelArt.Data.Models;
using PixelArt.Utils.Image;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PixelArt.Brushes
{
    public class FillOfCircleBrush : BaseBrush
    {
        public FillOfCircleBrush(BrushData brushItem) : base(brushItem) { }

        protected override List<PixelItem> GetPixelItems(Vector2Int point, PaletteItem palette, ImageData image)
        {
            var pixels = image.pixels;
            var width = image.width;
            var height = image.height;
            var radius = _brushItem.circleRadius;
            var radiusSqr = radius * radius;

            var result = new List<PixelItem>();

            var maxX = (int)Mathf.Clamp(point.x + radius, 0, width);
            var minX = (int)Mathf.Clamp(point.x - radius, 0, width);
            var maxY = (int)Mathf.Clamp(point.y + radius, 0, height);
            var minY = (int)Mathf.Clamp(point.y - radius, 0, height);

            var minRadiusX = minX - point.x;
            var maxRadiusX = maxX - point.x;

            for (var x = minRadiusX; x < maxRadiusX; x++)
            {
                var rx = Mathf.Clamp(point.x + x, minX, maxX);

                var hh = (int)Mathf.Sqrt(radiusSqr - x * x);
                var ph = Mathf.Clamp(point.y + hh, minY, maxY);
                var y =  Mathf.Clamp(point.y - hh, minY, maxY);

                for (; y < ph; y++) result.Add(pixels[ImageUtils.CalcIndex(rx, y, width)]);
            }

            return result;
        }
    }
}