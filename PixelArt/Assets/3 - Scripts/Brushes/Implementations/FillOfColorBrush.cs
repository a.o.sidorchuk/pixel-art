﻿using PixelArt.Brushes.Abstractions;
using PixelArt.Data.Models;
using PixelArt.Utils.Image;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.Brushes
{
    public class FillOfColorBrush : BaseBrush
    {
        public FillOfColorBrush(BrushData brushItem) : base(brushItem) { }
        
        protected override List<PixelItem> GetPixelItems(Vector2Int point, PaletteItem palette, ImageData image)
        {
            var result = new List<PixelItem>();
            var pixel = image.pixels[ImageUtils.CalcIndex(point.x, point.y, image.width)];

            if (pixel.paletteItem != palette) return result;

            result = image.pixels
                .Where(x => x.paletteItem == palette && !(x.isRealColor.HasValue && x.isRealColor.Value))
                .ToList();

            return result;
        }
    }
}