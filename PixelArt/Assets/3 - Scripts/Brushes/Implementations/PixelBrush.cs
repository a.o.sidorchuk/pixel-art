﻿using PixelArt.Brushes.Abstractions;
using PixelArt.Data.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.Brushes
{
    public class PixelBrush : BaseBrush
    {
        public PixelBrush(BrushData brushItem) : base(brushItem) { }

        protected override List<PixelItem> GetPixelItems(Vector2Int point, PaletteItem palette, ImageData image)
        {
            var pixel = image.GetPixel(point);
            return new List<PixelItem> { pixel };
        }
    }
}