﻿using PixelArt.Data;
using PixelArt.Data.Models;
using System;
using System.Collections.Generic;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace PixelArt.Game
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] LayerMask _imageLayer;

        [Space]
        [SerializeField] float _zoomSpeed = 100f;
        [SerializeField] float _moveSpeed = 100f;

        [Space]
        [SerializeField] Camera _cam;

        Vector3 _targetPos;

        ImageInfo _imageInfo;
        GameCameraParams _cameraParams;

        Vector3 _maxPos;
        Vector3 _minPos;

        [Space]
        [SerializeField] RangeInt _zoomRange;

        float _initZoomSpeed = 200f;

        public Camera Camera => _cam;

        List<IDisposable> _gameMsgSubs = new List<IDisposable>();

        private void Awake()
        {
            _imageInfo = AppStore.Instance.CurrentImageInfo;
            _cameraParams = _imageInfo.gameCameraParams;

            _minPos = Vector3.zero;
            _maxPos = new Vector3 { x = _imageInfo.imageData.width, y = _imageInfo.imageData.height };

            CameraInit();

            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.AutozoomStart)
                .Subscribe(AutozoomStartEvent));
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.EndGame)
                .Subscribe(EndGameEvent));
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.RestartGame)
                .Subscribe(RestartGameEvent));
        }

        private void OnEnable()
        {
            SceneManagement.PlayerInput.Instance.swipeEvent += SwipeCameraPosition;
            SceneManagement.PlayerInput.Instance.moveEvent += UpdateCameraPosition;
            SceneManagement.PlayerInput.Instance.zoomEvent += UpdateCameraZoom;
        }

        private void OnDisable()
        {
            SceneManagement.PlayerInput.Instance.swipeEvent -= SwipeCameraPosition;
            SceneManagement.PlayerInput.Instance.moveEvent -= UpdateCameraPosition;
            SceneManagement.PlayerInput.Instance.zoomEvent -= UpdateCameraZoom;
        }

        private void OnDestroy()
        {
            _gameMsgSubs.ForEach(x => x.Dispose());
            _gameMsgSubs.Clear();
        }

        private void Update()
        {
            var delta = _targetPos - transform.position;
            if (delta != Vector3.zero)
            {
                transform.position = Vector3.Lerp(transform.position, _targetPos, Time.deltaTime * 10);
            }
        }

        private void EndGameEvent(GameMessage obj = null)
        {
            var pos = transform.position;
            pos.x = _imageInfo.imageData.width / 2;
            pos.y = _imageInfo.imageData.height / 2;
            transform.position = _targetPos = pos;

            StartSmoothZoomIn(false);
        }

        private void RestartGameEvent(GameMessage msg)
        {
            StartSmoothZoomIn();
        }

        private void AutozoomStartEvent(GameMessage msg)
        {
            StartSmoothZoomIn((bool)msg.data);
        }

        private void CameraInit()
        {
            _zoomRange = _cameraParams.zoomRange;
            _cam.orthographicSize = _cameraParams.zoom;

            transform.position = _cameraParams.GetPosition();
            _targetPos = transform.position;

            if (_imageInfo.isCompleted)
                EndGameEvent();
            else
                StartSmoothZoomIn();
        }

        private void UpdateCameraZoom(float deltaZoom, LayerMask layerMask)
        {
            if ((_imageLayer & layerMask) == 0) return;

            var zoom = _cam.orthographicSize;

            zoom += deltaZoom * (Time.deltaTime * _zoomSpeed);
            zoom = Mathf.Clamp(zoom, _zoomRange.start, _zoomRange.end);

            _cam.orthographicSize = zoom;

            _cameraParams.SetZoom(_cam.orthographicSize);
        }

        private void SwipeCameraPosition(Vector2 swipeDelta, Vector3 worldPosition, LayerMask layerMask)
        {
            if ((_imageLayer & layerMask) == 0) return;

            UpdateCameraPosition(swipeDelta * 5f);
        }

        private void UpdateCameraPosition(Vector3 deltaPos, Vector2 screenDelta, Vector3 worldPosition, LayerMask layerMask)
        {
            if ((_imageLayer & layerMask) == 0) return;

            UpdateCameraPosition(deltaPos);
        }

        public void UpdateCameraPosition(Vector3 deltaPos)
        {
            var position = _targetPos;

            position += deltaPos * (Time.deltaTime * -Mathf.Clamp(_moveSpeed - (_cam.orthographicSize * .5f), 1, _moveSpeed));

            if (position.x > _maxPos.x) position.x = _maxPos.x;
            if (position.y > _maxPos.y) position.y = _maxPos.y;
            if (position.x < _minPos.x) position.x = _minPos.x;
            if (position.y < _minPos.y) position.y = _minPos.y;

            _targetPos = position;

            _cameraParams.SetPosition(transform.position);
        }

        private void StartSmoothZoomIn(bool maximize = true) => UniTask.ToCoroutine(async () =>
        {
            var deltaZoom = _zoomRange.length / _initZoomSpeed;

            deltaZoom *= maximize ? -1 : 1;
            var zoomValue = maximize ? _zoomRange.start : _zoomRange.end;

            while (_cam.orthographicSize != zoomValue)
            {
                UpdateCameraZoom(deltaZoom, _imageLayer);

                await UniTask.Yield();
            }

            MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.AutozoomComplete, null, null));
        });
    }
}