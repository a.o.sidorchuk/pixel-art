﻿using PixelArt.Data;
using PixelArt.Data.Models;
using PixelArt.SceneManagement;
using PixelArt.Tilemap;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.Game
{
    public class EndGameMenu : MonoBehaviour
    {
        [SerializeField] CanvasGroup _canvasGroup;
        [Space]
        [SerializeField] Button _backToMenuButton;
        [SerializeField] Button _historyViewButton;
        [SerializeField] Button _restartButton;
        [Space]
        [SerializeField] GameTilemapController _tilemapController;

        bool _historyIsPlaying;

        ImageInfo _imageInfo;

        IDisposable _gameMsgSub;
        CancellationDisposable _historyToken = new CancellationDisposable();

        private void Awake()
        {
            _imageInfo = AppStore.Instance.CurrentImageInfo;

            if (_imageInfo.isCompleted)
                EndGameEvent();
            else
                SetActive(false, 0);

            _gameMsgSub = MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.EndGame)
                .Subscribe(EndGameEvent);
        }

        private void OnEnable()
        {
            _backToMenuButton.onClick.AddListener(BackToMenuEvent);
            _historyViewButton.onClick.AddListener(PlayHistoryEvent);
            _restartButton.onClick.AddListener(RestartImageEvent);
        }

        private void OnDisable()
        {
            _backToMenuButton.onClick.RemoveListener(BackToMenuEvent);
            _historyViewButton.onClick.RemoveListener(PlayHistoryEvent);
            _restartButton.onClick.RemoveListener(RestartImageEvent);
        }

        private void OnDestroy()
        {
            _gameMsgSub.Dispose();
        }

        private void PlayHistoryEvent() => PlayHistory();

        private void PlayHistory(int startDalay = 0) => UniTask.ToCoroutine(async () =>
        {
            if (_historyIsPlaying) return;
            _historyIsPlaying = true;

            await UniTask.Delay(startDalay);

            // воспроизвести шаги закрашивания
            await _tilemapController.StartHistory(_historyToken = new CancellationDisposable());

            _historyIsPlaying = false;
        });

        private void RestartImageEvent() => UniTask.ToCoroutine(async () =>
        {
            _historyToken.Dispose();
            _historyIsPlaying = false;

            SetActive(false);

            await AppStore.ResetCurrentImageProgress();

            _tilemapController.ResetTilemaps();

            MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.RestartGame, null, null));
        });


        private void EndGameEvent(GameMessage msg = null)
        {
            _imageInfo.imageStatus = ImageStatus.Completed;

            SetActive(true);

            PlayHistory(500);
        }

        private void BackToMenuEvent()
        {
            _historyToken.Dispose();
            _historyIsPlaying = false;
            SceneController.StartMenu();
        }

        private void SetActive(bool state, float fadeDuration = .5f) => UniTask.ToCoroutine(async () =>
        {
            _canvasGroup.interactable = state;
            _canvasGroup.blocksRaycasts = state;
            await ScreenFader.FadeProcess(state ? 1 : 0, fadeDuration, _canvasGroup);
        });
    }
}