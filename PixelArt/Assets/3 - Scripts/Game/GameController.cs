﻿using PixelArt.Brushes;
using PixelArt.Data;
using PixelArt.Data.Models;
using PixelArt.Tilemap;
using PixelArt.UI.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;


namespace PixelArt.Game
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] LayerMask _layerCheck;
        [Space]
        [SerializeField] GameTilemapController _gameTilemapController;
        [SerializeField] SpecialControlPanel _specialControlPanel;

        Vector3 _currentWorldPosition;

        ImageData _imageData;

        PaletteItem _currentPaletteItem = null;
        BrushData _currentBrushItem = null;

        bool eventStarted = false;
        bool gameStarted = false;

        List<IDisposable> _gameMsgSubs = new List<IDisposable>();

        private void Awake()
        {
            var imageInfo = AppStore.Instance.CurrentImageInfo;
            _imageData = imageInfo.imageData;

            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.PickPaletteItem)
                .Subscribe(PickPaletteItemEvent));
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.PickBrushItem)
                .Subscribe(PickBrushItemEvent));
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.RestartGame)
                .Subscribe(RestartGameEvent));
        }

        private void RestartGameEvent(GameMessage obj)
        {
            gameStarted = true;
            SceneManagement.PlayerInput.Instance.SetMoveActiveState(true);
        }

        private void Start()
        {
            gameStarted = !AppStore.Instance.CurrentImageInfo.isCompleted;
            SceneManagement.PlayerInput.Instance.SetMoveActiveState(gameStarted);
        }

        private void OnEnable()
        {
            SceneManagement.PlayerInput.Instance.fingerUpEvent += FingerUpEvent;
            SceneManagement.PlayerInput.Instance.fingerDownEvent += FingerDownEvent;
            SceneManagement.PlayerInput.Instance.worldPositionEvent += SetWorldPositionEvent;
            SceneManagement.PlayerInput.Instance.tapEvent += TapEvent;
            _specialControlPanel.onJoystickUp += FingerUpEvent;
            _specialControlPanel.onJoystickDown += FingerDownEvent;
        }

        private void OnDisable()
        {
            SceneManagement.PlayerInput.Instance.fingerUpEvent -= FingerUpEvent;
            SceneManagement.PlayerInput.Instance.fingerDownEvent -= FingerDownEvent;
            SceneManagement.PlayerInput.Instance.worldPositionEvent -= SetWorldPositionEvent;
            SceneManagement.PlayerInput.Instance.tapEvent -= TapEvent;
            _specialControlPanel.onJoystickUp -= FingerUpEvent;
            _specialControlPanel.onJoystickDown -= FingerDownEvent;
        }

        private void OnDestroy()
        {
            _gameMsgSubs.ForEach(x => x.Dispose());
            _gameMsgSubs.Clear();
        }

        private void Update()
        {
            if (gameStarted && Mathf.Approximately(1, _imageData.GetProgressPercent()))
            {
                gameStarted = false;
                SceneManagement.PlayerInput.Instance.SetMoveActiveState(false);
                MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.EndGame, null, null));
            }
        }

        private void SetWorldPositionEvent(Vector3 worldPosition, LayerMask layerMask)
        {
            _currentWorldPosition = worldPosition;
        }

        private void FingerDownEvent(LayerMask layerMask)
        {
            if (eventStarted || (_layerCheck & layerMask) == 0 || _currentPaletteItem == null || _currentBrushItem == null) return;
            if (_currentBrushItem.playerInput == PlayerInput.TapAndMove)
            {
                SceneManagement.PlayerInput.Instance.SetMoveActiveState(false);

                FingerDown();
            }
        }

        private void FingerDownEvent()
        {
            SceneManagement.PlayerInput.Instance.SetMoveActiveState(false);

            if (eventStarted || _currentPaletteItem == null || _currentBrushItem == null) return;
            if (_currentBrushItem.playerInput == PlayerInput.Stick)
            {
                FingerDown();
            }
        }

        private void FingerDown()
        {
            Vector2Int getTmpPosition()
            {
                var isStick = _currentBrushItem.playerInput == PlayerInput.Stick;
                return isStick ? _specialControlPanel.GetAimPoint() : _currentWorldPosition.GetVector2Int();
            }

            eventStarted = true;

            UniTask.ToCoroutine(async () =>
            {
                var point = Vector2Int.zero;
                var tmpPoint = getTmpPosition();

                while (eventStarted)
                {
                    if (!point.Equals(tmpPoint))
                    {
                        point = tmpPoint;

                        await StartPaintingProcess(point, _imageData, _currentPaletteItem, _currentBrushItem, _gameTilemapController);
                    }

                    await UniTask.Yield();

                    tmpPoint = getTmpPosition();
                }
            });
        }

        private void FingerUpEvent(LayerMask layerMask) => FingerUpEvent();

        private void FingerUpEvent()
        {
            SceneManagement.PlayerInput.Instance.SetMoveActiveState(true);
            eventStarted = false;
        }

        private void TapEvent(Vector3 tapPosition, LayerMask layerMask)
        {
            if (eventStarted || (_layerCheck & layerMask) == 0 || _currentPaletteItem == null || _currentBrushItem == null) return;
            if (_currentBrushItem.playerInput == PlayerInput.Tap)
            {
                eventStarted = true;

                UniTask.ToCoroutine(async () =>
                {
                    var tapPositionInt = new Vector2Int(Mathf.FloorToInt(tapPosition.x), Mathf.FloorToInt(tapPosition.y));

                    await StartPaintingProcess(tapPositionInt, _imageData, _currentPaletteItem, _currentBrushItem, _gameTilemapController);

                    eventStarted = false;
                });
            }
        }

        private async UniTask StartPaintingProcess(Vector2Int point, ImageData image, PaletteItem palette, BrushData brushItem, GameTilemapController tilemap)
        {
            if (!brushItem.isPurchased || brushItem.isReloading || !image.CheckPosition(point)) return;

            await UniTask.Yield();

            var brush = BrushFactory.GetBrush(brushItem);
            var pixels = brush.GetPixels(point, palette, image);

            if (pixels == null || !pixels.Any()) return;

            await UniTask.Yield();

            tilemap.SetColor(pixels);

            await UniTask.Yield();

            // _TODO очистить область от тумана.
            // var fogPositions = brush.GetFogClearingPositions(point, image);
            // tilemap.RemoveFogTiles(fogPositions);

            // списание хранилища
            if (brushItem.paintStorageOn)
            {
                brushItem.RemainsInPaintStorageDecrement(pixels.Count());
            }

            // изменение состояния покупки
            if (brushItem.remainsInPaintStorage <= 0 && pixels.Any())
            {
                brushItem.SetPurchasedState(false);
                brushItem.StartReload();

                MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.ResetPickBrushItem, null, null));
            }

            var errorCheck = pixels.Any(x => !x.isRealColor.Value);

            // обработка ошибки
            if (brushItem.fineOn && errorCheck)
            {
                AppStore.Instance.UserProgress.AddFine(brushItem);

                // вибрация (_TODO сделать настройку в параметрах вкл/выкл)
                Handheld.Vibrate();
            }

            // начисление "капелек"
            if (brushItem.cost > 0 && !errorCheck)
            {
                AppStore.Instance.UserProgress.AddDroplets(brushItem.cost * pixels.Count());

                MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.BrushReloadIncrement, null, null));
            }

            AppStore.Instance.UserProgress.UpdateBrushSaveContainer(brushItem);
        }

        private void PickBrushItemEvent(GameMessage msg) => _currentBrushItem = (BrushData)msg.data;
        private void PickPaletteItemEvent(GameMessage msg) => _currentPaletteItem = (PaletteItem)msg.data;
    }
}