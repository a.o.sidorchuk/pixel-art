﻿using Lean.Gui;
using Lean.Touch;
using PixelArt.Utils;
using System;
using UnityEngine;

namespace PixelArt.SceneManagement
{
    public class PlayerInput : MonoSingleton<PlayerInput>
    {
        /// <summary>tap (мировая координата)</summary>
        public event Action<Vector3, LayerMask> tapEvent;

        /// <summary>finger down</summary>
        public event Action<LayerMask> fingerDownEvent;
        /// <summary>finger up</summary>
        public event Action<LayerMask> fingerUpEvent;

        /// <summary>move (мировое смещение, смещение на экране, мировая координата)</summary>
        public event Action<Vector3, Vector2, Vector3, LayerMask> moveEvent;
        /// <summary>swipe (смещение на экране, мировая координата)</summary>
        public event Action<Vector2, Vector3, LayerMask> swipeEvent;
        /// <summary>move (мировая коорината)</summary>
        public event Action<Vector3, LayerMask> worldPositionEvent;

        /// <summary>zoom (смещение с последнего кадра)</summary>
        public event Action<float, LayerMask> zoomEvent;

        bool _moveActive = false;

        private void TapEvent(LeanFinger finger)
        {
            if (!finger.IsOverGui) return;

            var worldPosition = finger.GetLastWorldPosition(0);

            tapEvent?.Invoke(worldPosition, GetLayerMask(finger));
        }

        private void PositionChangeEvent(LeanFinger finger)
        {
            if (!finger.IsOverGui) return;

            if (LeanTouch.Fingers.Count == 1 && finger.Old)
            {
                worldPositionEvent?.Invoke(finger.GetWorldPosition(0), GetLayerMask(finger));
            }
        }

        private void FingerSetEvent(LeanFinger finger)
        {
            if (!finger.IsOverGui) return;

            if (LeanTouch.Fingers.Count == 1 && finger.Old)
            {
                MoveEvent(finger, GetLayerMask(finger));
            }
        }

        private void ZoomEvent(LeanFinger finger)
        {
            if (!finger.IsOverGui) return;

            if (LeanTouch.Fingers.Count == 2)
            {
                ZoomEvent(GetLayerMask(finger));
            }
        }

        private void MoveEvent(LeanFinger finger, int layerMask)
        {
            moveEvent?.Invoke(finger.GetWorldDelta(0), finger.ScreenDelta, finger.GetWorldPosition(0), layerMask);
        }

        private void SwipeEvent(LeanFinger finger)
        {
            if (!finger.IsOverGui) return;

            var layerMask = GetLayerMask(finger);

            swipeEvent?.Invoke(finger.SwipeScreenDelta.normalized, finger.GetWorldPosition(0), layerMask);
        }

        private void ZoomEvent(int layerMask)
        {
            var pinchScale = 1 - LeanGesture.GetPinchScale();

            zoomEvent?.Invoke(pinchScale, layerMask);
        }

        private void FingerUpEvent(LeanFinger finger)
        {
            if (!finger.IsOverGui) return;

            fingerUpEvent?.Invoke(GetLayerMask(finger));
        }

        private void FingerDownEvent(LeanFinger finger)
        {
            if (!finger.IsOverGui) return;

            fingerDownEvent?.Invoke(GetLayerMask(finger));
        }

        private int GetLayerMask(LeanFinger finger)
        {
            return 1 << LeanTouch.RaycastGui(finger.ScreenPosition)[0].gameObject.layer;
        }

        private void OnEnable()
        {
            SetMoveActiveState(true);
            LeanTouch.OnFingerUp += FingerUpEvent;
            LeanTouch.OnFingerDown += FingerDownEvent;
            LeanTouch.OnFingerTap += TapEvent;
            LeanTouch.OnFingerSwipe += SwipeEvent;
            LeanTouch.OnFingerSet += PositionChangeEvent;
            LeanTouch.OnFingerSet += ZoomEvent;
        }

        private void OnDisable()
        {
            SetMoveActiveState(false);
            LeanTouch.OnFingerUp -= FingerUpEvent;
            LeanTouch.OnFingerDown -= FingerDownEvent;
            LeanTouch.OnFingerTap -= TapEvent;
            LeanTouch.OnFingerSwipe -= SwipeEvent;
            LeanTouch.OnFingerSet -= PositionChangeEvent;
            LeanTouch.OnFingerSet -= ZoomEvent;
        }

        public void SetMoveActiveState(bool state)
        {
            if (_moveActive == state) return;
            _moveActive = state;

            if (_moveActive)
                LeanTouch.OnFingerSet += FingerSetEvent;
            else
                LeanTouch.OnFingerSet -= FingerSetEvent;
        }
    }
}