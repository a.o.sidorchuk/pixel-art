﻿using PixelArt.Utils;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PixelArt.SceneManagement
{
    public partial class SceneController : MonoSingleton<SceneController>
    {
        [Header("Информация об основных сценах")]
        [SerializeField] SceneInfo _menuSceneInfo;
        [SerializeField] SceneInfo _gameSceneInfo;

        protected bool _loadingProcess;

        public bool LoadingProcess => _loadingProcess;

        public void LoadGameScene(SceneInfo sceneInfo = null)
        {
            if (!sceneInfo) sceneInfo = Instance._gameSceneInfo;
            Instance.LoadScene(sceneInfo.sceneName);
        }

        public void LoadMenuScene()
        {
            Instance.LoadScene(Instance._menuSceneInfo.sceneName);
        }

        public void LoadScene(string sceneName) => UniTask.ToCoroutine(async () =>
        {
            _loadingProcess = true;

            await ScreenFader.FadeSceneOut(FadeType.Loading);

            await SceneManager.LoadSceneAsync(sceneName);

            await ScreenFader.FadeSceneIn();

            _loadingProcess = false;
        });
    }
}