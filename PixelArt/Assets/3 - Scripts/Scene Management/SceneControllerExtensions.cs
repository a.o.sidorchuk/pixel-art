﻿using PixelArt.Data;
using PixelArt.Data.Models;
using PixelArt.SceneManagement;
using PixelArt.UI;
using PixelArt.Utils;
using SFB;
using UniRx.Async;
using UnityEngine;

namespace PixelArt.SceneManagement
{
    public partial class SceneController
    {
        public static void StartGame() => UniTask.ToCoroutine(async () =>
        {
            await ScreenFader.FadeSceneOut(FadeType.Loading);

            await AppStore.LoadCurrentImageData();

            Instance.LoadGameScene();
        });

        public static void StartMenu() => UniTask.ToCoroutine(async () =>
        {
            await ScreenFader.FadeSceneOut(FadeType.Loading);

            await AppStore.SaveCurrentImageData();

            Instance.LoadMenuScene();
        });

        public static void StartImageEditor() => UniTask.ToCoroutine(async () =>
        {
            await AppStore.LoadOriginalImage();

            ImageEditorMenu.Instance.SetActive(true);
        });


        public static bool StartByRandomImage()
        {
            var createResult = AppStore.FindRandomImageAndCreateImageInfo();

            if (createResult)
            {
                StartGame();
                return true;
            }

            return false;
        }

        public static async UniTask<bool> StartByUserImage()
        {
            bool createImageInfoAndStartGame(string[] filePaths)
            {
                if (filePaths.Length == 0) return false;

                var filePath = filePaths[0];

                if (string.IsNullOrEmpty(filePath)) return false;

                Debug.Log($"Image Path: {filePath}");

                var imageInfo = ImageInfo.CreteNew(filePath, ImageType.User);

                AppStore.Instance.AddImageInfoToProgress(imageInfo);

                return true;
            }

            bool? result = null;

#if UNITY_EDITOR
            var extensions = new[] { new ExtensionFilter("Image files", "png", "jpg", "jpeg") };
            StandaloneFileBrowser.OpenFilePanelAsync("Select an image", "", extensions, false, (imageFiles) =>
            {
                result = createImageInfoAndStartGame(imageFiles);
            });
#elif UNITY_ANDROID && !UNITY_EDITOR
            var permission = NativeGallery.GetImageFromGallery((path) =>
            {
                result = createImageInfoAndStartGame(new[] { path });
            }, "Select an image");
#endif

            while (!result.HasValue) await UniTask.Yield();

            if (result.Value) StartImageEditor();

            return result.Value;
        }

        public static void StartBySelectedImage(ImageInfo imageInfo)
        {
            AppStore.Instance.SetCurrentImageInfo(imageInfo);

            StartGame();
        }
    }
}