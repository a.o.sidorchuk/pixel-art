﻿using UnityEngine;

namespace PixelArt.SceneManagement
{
    [CreateAssetMenu(menuName = "PixelArt/SceneInfo")]
    public class SceneInfo : ScriptableObject
    {
        [SceneName] public string sceneName;
    }
}