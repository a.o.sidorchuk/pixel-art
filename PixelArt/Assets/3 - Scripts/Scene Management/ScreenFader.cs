﻿using PixelArt.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx.Async;
using UnityEngine;

namespace PixelArt.SceneManagement
{
    public enum FadeType { Default, Loading }

    [Serializable]
    public class ScreenFaderParam
    {
        public FadeType fadeType;
        public CanvasGroup canvasGroup;
    }

    public class ScreenFader : MonoSingleton<ScreenFader>
    {
        [SerializeField] float _fadeDuration = 1f;
        [SerializeField] List<ScreenFaderParam> _screenFaders = new List<ScreenFaderParam>();

        public bool IsFading { get; private set; }

        FadeType _currentType = FadeType.Default;

        public static async UniTask FadeSceneIn()
        {
            var canvasGroup = Instance.GetCurrentFadeScreen();

            if (!canvasGroup) return;

            await Instance.Fade(0f, canvasGroup);

            canvasGroup.gameObject.SetActive(false);
        }

        public static async UniTask FadeSceneOut(FadeType fadeType = FadeType.Default)
        {
            Instance._currentType = fadeType;

            var canvasGroup = Instance.GetCurrentFadeScreen();

            if (!canvasGroup) return;

            canvasGroup.gameObject.SetActive(true);

            await Instance.Fade(1f, canvasGroup);
        }


        private async UniTask Fade(float finalAlpha, CanvasGroup canvasGroup)
        {
            IsFading = true;

            canvasGroup.blocksRaycasts = true;

            await FadeProcess(finalAlpha, _fadeDuration, canvasGroup);

            canvasGroup.blocksRaycasts = false;

            IsFading = false;
        }

        private CanvasGroup GetCurrentFadeScreen() => _screenFaders
            .FirstOrDefault(x => x.fadeType == _currentType)
            .canvasGroup;

        public static async UniTask FadeProcess(float finalAlpha, float fadeDuration, CanvasGroup canvasGroup)
        {
            var fadeSpeed = Mathf.Abs(canvasGroup.alpha - finalAlpha) / fadeDuration;

            while (!Mathf.Approximately(canvasGroup.alpha, finalAlpha))
            {
                canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, finalAlpha, fadeSpeed * Time.deltaTime);

                await UniTask.Yield();
            }

            canvasGroup.alpha = finalAlpha;
        }
    }
}