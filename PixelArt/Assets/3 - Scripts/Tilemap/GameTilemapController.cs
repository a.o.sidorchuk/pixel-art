﻿using PixelArt.Data;
using PixelArt.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Tilemaps;
using TilemapObject = UnityEngine.Tilemaps.Tilemap;

namespace PixelArt.Tilemap
{
    public class GameTilemapController : MonoBehaviour
    {
        [SerializeField] TilemapObject _pixelTilemap;
        [SerializeField] TilemapObject _pixelBorderTilemap;
        [SerializeField] TilemapObject _numTilemap;
        [SerializeField] TilemapObject _fogTilemap;

        [Space]
        [SerializeField] Tile _pixelBorderTile;
        [SerializeField] Tile _coloredPixelTile;
        [SerializeField] Tile[] _numPixelTiles;

        [Space]
        [SerializeField] bool _fogOn = true; // _TODO включить туман, если когда нибудь сделаю
        [SerializeField] RuleTile _fogTile;

        Color _baseNumTileColor;
        Color _lightNumColor = new Color(.25f, .25f, .25f, 1);
        Color _darkNumColor = new Color(1, .988f, .9686f, 1);

        GameCameraParams _cameraParams => AppStore.Instance.CurrentImageInfo.gameCameraParams;
        RangeInt _alphaChangeCameraSizeSegment = new RangeInt(18, 12);
        float _prevAlphaChanelValue;

        IDisposable _gameMsgSub;

        private void Awake()
        {
            _coloredPixelTile.flags &= ~TileFlags.LockColor;
            foreach (var tile in _numPixelTiles) tile.flags &= ~TileFlags.LockColor;

            Init(AppStore.Instance.CurrentImageInfo.imageData, _fogOn);

            _prevAlphaChanelValue = CalcAlphaChanelForNumTiles();

            SetAlphaChanelForNumTiles(_prevAlphaChanelValue);

            _gameMsgSub = MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.PickPaletteItem)
                .Subscribe(PickPaletteItemEvent);
        }

        private void PickPaletteItemEvent(GameMessage msg)
        {
            var paletteItem = (PaletteItem)msg.data;

            // _TODO "подсвечивать" пиксели для помощи игроку в поиске незакрашенных

            // (!) ранее выделеные пиксели - поменять им цвет обратно на белый
            // (учесть закрашенные за время выбора цвета пиксели)

            // получить список пикселей с новым цветом

            // "выделить" незакрашенные пиксели с новым цветом (например светло-серым)
            // выделить "тихо" не отмечая что пиксель закрашен
        }

        public async UniTask StartHistory(CancellationDisposable historyToken)
        {
            var pixelsForPlay = AppStore.Instance.CurrentImageInfo.imageData.pixels
                .Where(x => !x.paletteItem.isWhiteColor)
                .OrderBy(x => x.pickId)
                .ToList();

            var count = pixelsForPlay.Count;
            var step = Mathf.CeilToInt(count / 1000f);

            var positions = pixelsForPlay.Select(x => x.position).ToArray();
            var colors = pixelsForPlay.Select(x => x.paletteItem.color).ToArray();
            var whiteTiles = Enumerable.Range(0, count).Select(x => _coloredPixelTile).ToArray();

            // сброс до белого
            _pixelTilemap.SetTiles(positions, new TileBase[count]);
            _pixelTilemap.SetTiles(positions, whiteTiles);

            // воспроизводим историю
            for (var i = 0; i < count; i++)
            {
                _pixelTilemap.SetColor(positions[i], colors[i]);

                if (!historyToken.IsDisposed && (i % step) == 0) await UniTask.Yield();
            }
        }

        private void Update()
        {
            var newAlphaValue = CalcAlphaChanelForNumTiles();
            if (!Mathf.Approximately(newAlphaValue, _prevAlphaChanelValue) &&
                Mathf.Abs(_prevAlphaChanelValue - newAlphaValue) > 0.05f)
            {
                _prevAlphaChanelValue = newAlphaValue;
                SetAlphaChanelForNumTiles(newAlphaValue);
            }
        }

        private void Init(ImageData imageData, bool fogOn)
        {
            var pixels = imageData.pixels;
            var pixelCount = pixels.Count;

            var positions = new Vector3Int[pixelCount];
            var pixelTiles = new Tile[pixelCount];
            var pixelBorderTiles = new Tile[pixelCount];
            var numTiles = new Tile[pixelCount];
            var fogTiles = new RuleTile[pixelCount];

            for (var i = 0; i < pixelCount; i++)
            {
                var pixel = pixels[i];

                positions[i] = pixel.position;
                pixelTiles[i] = _coloredPixelTile;

                if (!pixel.isRealColor.HasValue)
                {
                    fogTiles[i] = _fogTile;

                    var isWhiteColor = pixel.paletteItem.isWhiteColor;
                    pixelBorderTiles[i] = isWhiteColor ? null : _pixelBorderTile;
                    numTiles[i] = isWhiteColor ? null : _numPixelTiles[pixel.paletteItem.id];
                }
                else if (!pixel.isRealColor.Value)
                {
                    numTiles[i] = _numPixelTiles[pixel.paletteItem.id];
                }
            }

            _baseNumTileColor = Color.white;

            _numTilemap.SetTiles(positions, numTiles);
            _pixelTilemap.SetTiles(positions, pixelTiles);
            _pixelBorderTilemap.SetTiles(positions, pixelBorderTiles);
            if (fogOn) _fogTilemap.SetTiles(positions, fogTiles);

            // восстановить уже закрашенное
            foreach (var pixel in pixels.Where(x => x.isRealColor.HasValue))
            {
                SetTileColor(pixel);
            }
        }

        private void SetTileColor(PixelItem pixel)
        {
            var color = pixel.GetCurrentColor();
            _pixelTilemap.SetColor(pixel.position, color);
            _numTilemap.SetColor(pixel.position, color.IsLight() ? _darkNumColor : _lightNumColor);
        }

        public void ResetTilemaps()
        {
            _numTilemap.ClearAllTiles();
            _pixelTilemap.ClearAllTiles();
            _pixelBorderTilemap.ClearAllTiles();
            _fogTilemap.ClearAllTiles();

            Init(AppStore.Instance.CurrentImageInfo.imageData, _fogOn);
        }

        public void SetColor(IEnumerable<PixelItem> pixels)
        {
            var positions = pixels.Select(x => x.position).ToArray();
            var posCount = positions.Length;

            RemoveTiles(_pixelBorderTilemap, positions);

            foreach (var pixel in pixels)
            {
                if (pixel.isRealColor.Value) RemoveTile(_numTilemap, pixel.position);

                SetTileColor(pixel);
            }
        }

        public void RemoveFogTiles(Vector3Int[] cellPositions)
        {
            RemoveTiles(_fogTilemap, cellPositions);
        }

        /// <summary> Закраска выбранного пикселя </summary>
        public void SetColor(PixelItem pixel, Color mainColor)
        {
            RemoveTile(_pixelBorderTilemap, pixel.position);
            RemoveTile(_pixelTilemap, pixel.position);

            _pixelTilemap.SetTile(pixel.position, _coloredPixelTile);

            if (pixel.isRealColor.Value)
                RemoveTile(_numTilemap, pixel.position);
            else
                mainColor.a /= 2;

            _pixelTilemap.SetColor(pixel.position, mainColor);
        }

        /// <summary> Удалить ячейки с указанного тайлмапа </summary>
        public void RemoveTiles(TilemapObject tilemap, Vector3Int[] cellPositions)
        {
            tilemap.SetTiles(cellPositions, new TileBase[cellPositions.Length]);
        }

        /// <summary> Удалить ячейку с указанного тайлмапа </summary>
        public void RemoveTile(TilemapObject tilemap, Vector3Int cellPosition)
        {
            tilemap.SetTile(cellPosition, null);
        }

        /// <summary> Изменить видимость числовых значений </summary>
        public void SetAlphaChanelForNumTiles(float alphaValue)
        {
            _baseNumTileColor.a = alphaValue;
            _numTilemap.color = _baseNumTileColor;
            _pixelBorderTilemap.color = _baseNumTileColor;
        }

        private float CalcAlphaChanelForNumTiles()
        {
            if (_cameraParams.zoom < _alphaChangeCameraSizeSegment.start) return 1f;
            if (_cameraParams.zoom > _alphaChangeCameraSizeSegment.end) return 0;

            return 1 - (_cameraParams.zoom - _alphaChangeCameraSizeSegment.start) / _alphaChangeCameraSizeSegment.length;
        }

        private void OnDestroy() => _gameMsgSub.Dispose();
    }
}