﻿using PixelArt.Data;
using PixelArt.Data.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI.Game
{
    public class AutozoomButton : MonoBehaviour
    {
        [SerializeField] Button _button;
        
        [Space]
        [SerializeField] Image _icon;
        [SerializeField] Sprite _maxZoomIcon;
        [SerializeField] Sprite _minZoomIcon;

        bool _maxZoomState => _cameraParams.GetZoomPercent() > .2f;
        bool _lastZoomState;

        GameCameraParams _cameraParams;

        IDisposable _gameMsgSub;

        private void Awake()
        {
            _button.onClick.AddListener(ClickEvent);
            _gameMsgSub = MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.AutozoomComplete)
                .Subscribe(AutozoomCompleteEvent);
        }

        private void Start()
        {
            _cameraParams = AppStore.Instance.CurrentImageInfo.gameCameraParams;

            _lastZoomState = _maxZoomState;
        }

        private void Update()
        {
            var currentZoomState = _maxZoomState;

            if (_lastZoomState != currentZoomState)
            {
                _icon.sprite = currentZoomState ? _maxZoomIcon : _minZoomIcon;
                _lastZoomState = currentZoomState;
            }
        }

        private void OnDestroy()
        {
            _gameMsgSub.Dispose();
        }

        private void AutozoomCompleteEvent(GameMessage obj)
        {
            _button.interactable = true;
        }

        private void ClickEvent()
        {
            _button.interactable = false;
            MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.AutozoomStart, !_lastZoomState, null));
        }
    }
}