﻿using PixelArt.Data;
using PixelArt.Data.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI.Game
{
    public class BrushItemButton : MonoBehaviour
    {
        public event Action<BrushItemButton> onClick;

        [SerializeField] Button _button;
        [SerializeField] Button _buyButton;
        [Space]
        [SerializeField] Sprite _activeSprite;
        [SerializeField] Sprite _inactiveSprite;
        [Space]
        [SerializeField] Image _bgImage;
        [SerializeField] Image _iconImage;
        [Space]
        [SerializeField] Image _reloadImage;
        [SerializeField] Image _storageImage;
        [Space]
        [SerializeField] GameObject _costPanel;
        [SerializeField] Text _costText;

        public BrushData BrushData { get; private set; }

        bool _isActive = false;

        public void Init(BrushData brush)
        {
            BrushData = brush;

            BrushData.icon.LoadAssetAsync<Sprite>().Completed += x => _iconImage.sprite = x.Result;

            if (BrushData.cost < 0)
                _costText.text = Mathf.Abs(BrushData.cost).ToString();
            else
                _costPanel.SetActive(false);

            _button.onClick.AddListener(ClickEvent);

            _buyButton.gameObject.SetActive(false);
            _buyButton.onClick.AddListener(BuyEvent);
        }

        private void Update()
        {
            if (_isActive && !BrushData.isPurchased && BrushData.cost < 0)
            {
                _buyButton.gameObject.SetActive(!BrushData.isReloading);
            }
        }

        public void ClickEvent()
        {
            SetActive(true);

            onClick?.Invoke(this);
        }

        private void BuyEvent()
        {
            if (BrushData.isReloading) return;

            var result = AppStore.Instance.UserProgress.BrushPurchasedProcess(BrushData);

            _buyButton.gameObject.SetActive(!result);
        }

        public void UpdateReloadPercent()
        {
            _reloadImage.fillAmount = BrushData.GetReloadPercent();
        }

        public void UpdateStoragePercent()
        {
            _storageImage.fillAmount = BrushData.GetStoragePercent();
        }

        public void SetActive(bool state)
        {
            _isActive = state;

            // при переключении скрыть кнопку покупки
            if (!_isActive) _buyButton.gameObject.SetActive(false);

            var sprite = _isActive ? _activeSprite : _inactiveSprite;
            _bgImage.sprite = sprite;
            _storageImage.sprite = sprite;
            _reloadImage.sprite = sprite;
        }
    }
}