﻿using PixelArt.Data;
using PixelArt.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI.Game
{
    public class BrushPanel : MonoBehaviour
    {
        [SerializeField] RectTransform _rectTransform;
        [SerializeField] GameObject _brushItemPrefab;
        [Space]
        [SerializeField] List<BrushData> _brushes = new List<BrushData>();

        bool _isActive;

        List<BrushItemButton> _brushItemButtons = new List<BrushItemButton>();
        BrushItemButton _currentBrushButton;

        List<IDisposable> _gameMsgSubs = new List<IDisposable>();

        public void Init()
        {
            UpdateBrushesInfo();

            foreach (var brush in _brushes)
            {
                var brushBtn = Instantiate(_brushItemPrefab, _rectTransform).GetComponent<BrushItemButton>();

                brushBtn.Init(brush);

                _brushItemButtons.Add(brushBtn);

                brushBtn.onClick += BrushClickEvent;
            }

            // кликаем по первой кисти для ее активации
            if (_brushItemButtons.Any()) _brushItemButtons[0].ClickEvent();

            SetActive(true);
        }

        private void OnEnable()
        {
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.BrushReloadIncrement)
                .Subscribe(BrushReloadIncrementEvent));
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.ResetPickBrushItem)
                .Subscribe(ResetPickBrushItemEvent));
        }

        private void OnDisable()
        {
            _gameMsgSubs.ForEach(x => x.Dispose());
            _gameMsgSubs.Clear();
        }

        private void ResetPickBrushItemEvent(GameMessage obj)
        {
            // кликаем по первой кисти для ее активации
            if (_brushItemButtons.Any()) _brushItemButtons[0].ClickEvent();
        }

        private void BrushReloadIncrementEvent(GameMessage obj)
        {
            var userProgress = AppStore.Instance.UserProgress;

            _brushItemButtons.ForEach(x =>
            {
                x.BrushData.ReloadCountIncrement();

                userProgress.UpdateBrushSaveContainer(x.BrushData);
            });

        }

        private void UpdateBrushesInfo()
        {
            var playerBrushes = AppStore.Instance.UserProgress.brushSaves;

            foreach (var item in playerBrushes)
            {
                var brush = _brushes.FirstOrDefault(x => x.id == item.id);

                if (brush == null) continue;

                brush.SetParams(item);
            }
        }

        private void Update()
        {
            foreach (var item in _brushItemButtons)
            {
                item.UpdateReloadPercent();
                item.UpdateStoragePercent();
            }
        }

        private void BrushClickEvent(BrushItemButton button)
        {
            if (button == _currentBrushButton) return;
            if (_currentBrushButton != null) _currentBrushButton.SetActive(false);
            _currentBrushButton = button;

            MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.PickBrushItem, button.BrushData, null));
        }

        public void OpenStateChange()
        {
            SetActive(!_isActive);
        }

        public void SetActive(bool state)
        {
            _isActive = state;

            // _TODO показать/скрыть панель кистей (сделать анимацию)
            gameObject.SetActive(_isActive);
        }
    }
}