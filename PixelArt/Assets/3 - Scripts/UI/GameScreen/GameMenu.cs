﻿using PixelArt.Data;
using PixelArt.Data.Models;
using PixelArt.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace PixelArt.UI.Game
{
    public class GameMenu : MonoBehaviour
    {
        [SerializeField] CanvasGroup _canvasGroup;

        [Header("Title Panel")]
        [SerializeField] Button _backToMenuButton;
        [Space]
        [SerializeField] Image _progressFillImage;
        [SerializeField] Text _progressText;
        [Space]
        [SerializeField] GameObject _errorPanel;
        [SerializeField] Text _errorText;
        [Space]
        [SerializeField] Text _dropletAmountText;

        [Header("Palette Panel")]
        [SerializeField] PalettePanel _palettePanel;

        [Header("Brush Panel")]
        [SerializeField] Button _openBrushPanelButton;
        [SerializeField] BrushPanel _brushPanel;

        readonly ProgressData _progressData;
        readonly ImageData _imageData;

        List<IDisposable> _gameMsgSubs = new List<IDisposable>();

        public GameMenu()
        {
            _progressData = AppStore.Instance.UserProgress;
            _imageData = AppStore.Instance.CurrentImageInfo?.imageData;
        }

        private void Awake()
        {
            _backToMenuButton.onClick.AddListener(BackToMenu);
            _openBrushPanelButton.onClick.AddListener(OpenBrushMenu);

            _brushPanel.Init();

            var paletteItems = _imageData.palette
                .Where(p => !p.isWhiteColor)
                .ToArray();

            _palettePanel.Init(paletteItems);

            // скрыть если картинка пройдена
            SetActive(!AppStore.Instance.CurrentImageInfo.isCompleted, 0);

            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.EndGame)
                .Subscribe(EndGameEvent));
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.RestartGame)
                .Subscribe(RestartGameEvent));
        }

        private void Update()
        {
            _dropletAmountText.text = _progressData.dropletAmount.ToString();

            var progress = _imageData.GetProgressPercent();
            _progressFillImage.fillAmount = progress;
            _progressText.text = $"{Mathf.CeilToInt(progress * 100f)}%";

            if (_imageData.errorPixelCount > 0)
            {
                _errorPanel.SetActive(true);
                _errorText.text = $"{_imageData.errorPixelCount}";
            }
            else
            {
                _errorPanel.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            _gameMsgSubs.ForEach(x => x.Dispose());
            _gameMsgSubs.Clear();
        }

        private void RestartGameEvent(GameMessage obj)
        {
            _palettePanel.RestartGameEvent();
            SetActive(true);
        }

        private void EndGameEvent(GameMessage msg = null) => SetActive(false);

        private void SetActive(bool state, float fadeDuration = .5f) => UniTask.ToCoroutine(async () =>
        {
            _canvasGroup.interactable = state;
            _canvasGroup.blocksRaycasts = state;
            await ScreenFader.FadeProcess(state ? 1 : 0, fadeDuration, _canvasGroup);
        });

        private void OpenBrushMenu() => _brushPanel.OpenStateChange();

        private void BackToMenu() => SceneController.StartMenu();
    }
}