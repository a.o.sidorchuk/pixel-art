﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI.Game
{
    public class PageItem : MonoBehaviour
    {
        [SerializeField] Image _image;

        [Space]
        [SerializeField] Color _activeColor;
        [SerializeField] Color _inactiveColor;
        [SerializeField] float _activeScaleValue = 1.3f;

        public void Init() => SetActive(false);

        public void SetActive(bool state)
        {
            _image.color = state ? _activeColor : _inactiveColor;
            _image.rectTransform.localScale = state ? new Vector2(_activeScaleValue, _activeScaleValue) : Vector2.one;
        }
    }
}