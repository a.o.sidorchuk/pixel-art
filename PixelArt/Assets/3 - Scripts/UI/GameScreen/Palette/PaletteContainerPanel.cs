﻿using PixelArt.Data.Models;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.UI.Game
{
    public class PaletteContainerPanel : MonoBehaviour
    {
        [SerializeField] RectTransform _rectTransform;
        [Space]
        [SerializeField] GameObject _paletteItemRowPrefab;

        public List<PaletteItemButton> Init(PaletteItem[] paletteItems)
        {
            var uiPaletteItems = new List<PaletteItemButton>();

            var itemInRowNum = 5;
            var rowCount = Mathf.CeilToInt(paletteItems.Length * 1.0f / itemInRowNum);

            for (var i = rowCount - 1; i >= 0; i--)
            {
                var items = paletteItems.Skip(i * itemInRowNum).Take(itemInRowNum).ToArray();

                var itemRow = Instantiate(_paletteItemRowPrefab, _rectTransform).GetComponent<PaletteItemRow>();

                uiPaletteItems.AddRange(itemRow.Init(items));
            }

            return uiPaletteItems;
        }
    }
}