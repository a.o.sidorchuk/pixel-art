﻿using PixelArt.Data.Models;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI.Game
{
    public class PaletteItemButton : MonoBehaviour
    {
        public event Action<PaletteItemButton> onClick;

        [SerializeField] Button _button;
        [Space]
        [SerializeField] Image _bgImage;
        [SerializeField] Sprite _bgSprite;
        [SerializeField] Image _activeImage;
        [SerializeField] Sprite _activeSprite;
        [Space]
        [SerializeField] Image _numImage;
        [SerializeField] Text _numText;
        [Space]
        [SerializeField] Image _progressImage;
        [SerializeField] Text _progressText;
        [SerializeField] Sprite _completeSprite;
        Sprite _numSprite;

        Color _lightNumColor = new Color(.25f, .25f, .25f, 1);
        Color _darkNumColor = new Color(1, .988f, .9686f, 1);

        public PaletteItem PaletteItem { get; private set; }

        private void Update()
        {
            var progress = PaletteItem.GetProgress();

            _progressImage.fillAmount = progress;
            _progressText.text = $"{Mathf.CeilToInt(progress * 100f)}%";

            if (progress == 1)
            {
                SetCompleteState();
            }
        }

        public void Init(PaletteItem paletteItem)
        {
            PaletteItem = paletteItem;

            _bgImage.sprite = _bgSprite;
            _activeImage.sprite = _activeSprite;

            _bgImage.color = PaletteItem.color;
            
            _numText.text = $"{PaletteItem.id + 1}";
            _numImage.sprite = _completeSprite;

            var color = PaletteItem.color.IsLight() ? _darkNumColor : _lightNumColor;
            _numText.color = _numImage.color = _progressText.color = _activeImage.color = color;
            
            _button.onClick.AddListener(() => { SetActive(true); onClick?.Invoke(this); });
        }

        public void SetActive(bool state)
        {
            _activeImage.gameObject.SetActive(state);
            //var sprite = state ? _activeSprite : _bgSprite;
            //_bgImage.sprite = sprite;
            //_progressImage.sprite = sprite;
        }

        public void SetCompleteState(bool state = true)
        {
            enabled = !state;
            _button.interactable = !state;
            _numText.gameObject.SetActive(!state);
            _numImage.gameObject.SetActive(state);
        }

        public void Invoke()
        {
            _button.onClick.Invoke();
        }
    }
}