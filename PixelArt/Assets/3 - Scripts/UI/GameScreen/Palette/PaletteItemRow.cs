﻿using PixelArt.Data.Models;
using System.Collections.Generic;
using UnityEngine;

namespace PixelArt.UI.Game
{
    public class PaletteItemRow : MonoBehaviour
    {
        [SerializeField] RectTransform _rectTransform;
        [Space]
        [SerializeField] GameObject _paletteItemPrefab;

        public List<PaletteItemButton> Init(PaletteItem[] items)
        {
            var paletteItems = new List<PaletteItemButton>();

            for (var i = 0; i < items.Length; i++)
            {
                var item = Instantiate(_paletteItemPrefab, _rectTransform).GetComponent<PaletteItemButton>();

                item.Init(items[i]);

                paletteItems.Add(item);
            }

            return paletteItems;
        }
    }
}