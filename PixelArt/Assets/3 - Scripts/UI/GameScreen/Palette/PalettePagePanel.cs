﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.UI.Game
{
    public class PalettePagePanel : MonoBehaviour
    {
        [SerializeField] RectTransform _rectTransform;

        [Space]
        [SerializeField] GameObject _pageItemPregab;

        List<PageItem> _pageItems = new List<PageItem>();

        int _currentPageIndex = 0;

        public float Height => _rectTransform.rect.height;

        public void Init(int pageCount, int currentPageIndex)
        {
            if (pageCount <= 1) return; 

            for (var i = 0; i < pageCount; i++)
            {
                var item = Instantiate(_pageItemPregab, _rectTransform).GetComponent<PageItem>();

                item.Init();

                _pageItems.Add(item);
            }

            SetPageIndex(currentPageIndex);
        }

        public void SetPageIndex(int index)
        {
            if (!_pageItems.Any()) return;

            _pageItems[_currentPageIndex].SetActive(false);

            _pageItems[index].SetActive(true);

            _currentPageIndex = index;
        }
    }
}