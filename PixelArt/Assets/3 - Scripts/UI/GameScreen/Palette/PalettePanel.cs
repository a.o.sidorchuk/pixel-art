﻿using PixelArt.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace PixelArt.UI.Game
{
    public class PalettePanel : MonoBehaviour
    {
        [Header("Palette Containers")]
        [SerializeField] RectTransform _rectTransform;
        [Space]
        [SerializeField] GameObject _containerPrefab;
        [Header("Pages")]
        [SerializeField] PalettePagePanel _pagePanel;

        List<PaletteContainerPanel> _paletteContainers = new List<PaletteContainerPanel>();
        int _currentContainerIndex = 0;

        List<PaletteItemButton> _paletteButtons = new List<PaletteItemButton>();
        PaletteItemButton _currentPaletteItemBtn = null;


        public void Init(PaletteItem[] paletteItems)
        {
            var itemInPageNum = 10;

            var pageCount = Mathf.CeilToInt(paletteItems.Length * 1.0f / itemInPageNum);

            for (var i = 0; i < pageCount; i++)
            {
                var items = paletteItems.Skip(i * itemInPageNum).Take(itemInPageNum).ToArray();

                var container = Instantiate(_containerPrefab, _rectTransform).GetComponent<PaletteContainerPanel>();

                _paletteButtons.AddRange(container.Init(items));

                _paletteContainers.Add(container);
            }

            _pagePanel.Init(pageCount, _currentContainerIndex);

            SetPage(_currentContainerIndex);

            _paletteButtons.ForEach(p => p.onClick += PickPaletteItemEvent);
            _paletteButtons.FirstOrDefault(x => x.PaletteItem.id == 0).Invoke();
        }

        private void OnEnable()
        {
            SceneManagement.PlayerInput.Instance.swipeEvent += SwipePanel;
            SceneManagement.PlayerInput.Instance.moveEvent += MovePanel;
            SceneManagement.PlayerInput.Instance.fingerUpEvent += FingerUpEvent;
        }

        private void OnDisable()
        {
            SceneManagement.PlayerInput.Instance.swipeEvent += SwipePanel;
            SceneManagement.PlayerInput.Instance.moveEvent -= MovePanel;
            SceneManagement.PlayerInput.Instance.fingerUpEvent -= FingerUpEvent;
        }

        private void PickPaletteItemEvent(PaletteItemButton btn)
        {
            if (_currentPaletteItemBtn == btn) return;
            if (_currentPaletteItemBtn != null) _currentPaletteItemBtn.SetActive(false);
            _currentPaletteItemBtn = btn;

            MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.PickPaletteItem, btn.PaletteItem, null));
        }

        private void SwipePanel(Vector2 swipeDelta, Vector3 worldPosition, LayerMask layerMask)
        {
            var containerWidth = _rectTransform.rect.width;
            MovePanel(Vector3.zero, swipeDelta * containerWidth * .1f, worldPosition, layerMask);
            FingerUpEvent(layerMask);
        }

        private void MovePanel(Vector3 worldDelta, Vector2 deltaPosition, Vector3 worldPosition, LayerMask layerMask)
        {
            if ((1 << gameObject.layer & layerMask) == 0) return;

            var position = _rectTransform.anchoredPosition;
            position += Vector2.right * deltaPosition * 1.25f;
            _rectTransform.anchoredPosition = position;
        }

        public void RestartGameEvent()
        {
            _paletteButtons.ForEach(x => x.SetCompleteState(false));
        }

        private void FingerUpEvent(LayerMask layerMask)
        {
            var containerWidth = -_rectTransform.rect.width;
            var idlingWidth = containerWidth * .1f;
            var position = _rectTransform.anchoredPosition;
            var startContainerPosition = _currentContainerIndex * containerWidth;

            var (nextPage, prevPage) = CalcPageIndex();

            if (position.x < (startContainerPosition + idlingWidth))
            {
                SetPage(nextPage);
                position.x = nextPage * containerWidth;
            }
            else if (position.x > (startContainerPosition - idlingWidth))
            {
                SetPage(prevPage);
                position.x = prevPage * containerWidth;
            }
            else
                position.x = startContainerPosition;

            // _TODO запустить быстрое, но плавное смещение к position
            _rectTransform.anchoredPosition = position;
        }

        private void SetPage(int pageIndex)
        {
            _currentContainerIndex = pageIndex;

            _pagePanel.SetPageIndex(_currentContainerIndex);
        }

        private (int, int) CalcPageIndex()
        {
            var nextPage = _currentContainerIndex + 1;
            if (nextPage >= _paletteContainers.Count) nextPage--;

            var prevPage = _currentContainerIndex - 1;
            if (prevPage < 0) prevPage = 0;

            return (nextPage, prevPage);
        }
    }
}