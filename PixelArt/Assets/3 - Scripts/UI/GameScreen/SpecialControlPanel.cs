﻿using Lean.Gui;
using PixelArt.Data.Models;
using PixelArt.Game;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI.Game
{
    public class SpecialControlPanel : MonoBehaviour
    {
        public Action onJoystickDown;
        public Action onJoystickUp;

        [SerializeField] CameraController _cameraController;
        [SerializeField] [Range(0.01f, 0.5f)] float _cameraSpeed = 0.025f; 
        [Space]
        [SerializeField] Image _aimImage;
        [SerializeField] LeanJoystick _leanJoystick;

        List<IDisposable> _gameMsgSubs = new List<IDisposable>();

        private void Awake()
        {
            SetActive(false);

            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.PickBrushItem)
                .Subscribe(PickBrushItemEvent));
            _gameMsgSubs.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.ResetPickBrushItem)
                .Subscribe(ResetPickBrushItemEvent));

            _aimImage.transform.position = new Vector3
            {
                x = Screen.width / 2f,
                y = Screen.height / 2f
            };
        }

        private void OnEnable()
        {
            _leanJoystick.OnSet.AddListener(CameraUpdate);
            _leanJoystick.OnDown.AddListener(JoystickDownEvent);
            _leanJoystick.OnUp.AddListener(JoystickUpEvent);
        }

        private void OnDisable()
        {
            _leanJoystick.OnSet.RemoveListener(CameraUpdate);
            _leanJoystick.OnDown.RemoveListener(JoystickDownEvent);
            _leanJoystick.OnUp.RemoveListener(JoystickUpEvent);
        }

        private void JoystickUpEvent() => onJoystickUp?.Invoke();

        private void JoystickDownEvent() => onJoystickDown?.Invoke();

        private void CameraUpdate(Vector2 x) => _cameraController.UpdateCameraPosition(-x * _cameraSpeed);

        private void OnDestroy()
        {
            _gameMsgSubs.ForEach(x => x.Dispose());
            _gameMsgSubs.Clear();
        }

        public Vector2Int GetAimPoint()
        {
            var vector3 = _cameraController.Camera.ScreenToWorldPoint(_aimImage.rectTransform.position);
            return vector3.GetVector2Int();
        }

        private void PickBrushItemEvent(GameMessage msg)
        {
            SetActive(((BrushData)msg.data).playerInput == PlayerInput.Stick);
        }

        private void ResetPickBrushItemEvent(GameMessage obj)
        {
            _leanJoystick.OnPointerUp(_leanJoystick.Pointer);
        }

        public void SetActive(bool state)
        {
            _aimImage.gameObject.SetActive(state);
            _leanJoystick.gameObject.SetActive(state);
        }
    }
}