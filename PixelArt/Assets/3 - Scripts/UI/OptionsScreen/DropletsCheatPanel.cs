﻿using PixelArt.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI.Options
{
    public class DropletsCheatPanel : MonoBehaviour
    {
        [SerializeField] Text _dropletsAmount;
        [SerializeField] Button _addDropletsButton;
        [SerializeField] Button _removeDropletsButton;

        public bool _isSaving = false;

        private void OnEnable()
        {
            _removeDropletsButton.onClick.AddListener(RemoveDropletsEvent);
            _addDropletsButton.onClick.AddListener(AddDropletsEvent);
        }

        private void OnDisable()
        {
            _removeDropletsButton.onClick.RemoveListener(RemoveDropletsEvent);
            _addDropletsButton.onClick.RemoveListener(AddDropletsEvent);
        }

        private void RemoveDropletsEvent() => UniTask.ToCoroutine(async () =>
        {
            if (_isSaving) return;
            _isSaving = true;

            AppStore.Instance.UserProgress.AddDroplets(-1000);
            await AppStore.Instance.SaveProgressData();

            _isSaving = false;
        });


        private void AddDropletsEvent() => UniTask.ToCoroutine(async () =>
        {
            if (_isSaving) return;
            _isSaving = true;

            AppStore.Instance.UserProgress.AddDroplets(1000);
            await AppStore.Instance.SaveProgressData();

            _isSaving = false;
        });

        private void Update()
        {
            _dropletsAmount.text = AppStore.Instance.UserProgress.dropletAmount.ToString();
        }
    }
}