﻿using PixelArt.Data.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI
{
    public class DeleteImageDialog : MonoBehaviour
    {
        [SerializeField] Button _acceptButton;
        [SerializeField] Button _cancelButton;

        ImageInfo _imageInfo;

        public void Init(ImageInfo imageInfo)
        {
            _imageInfo = imageInfo;
            CancelEvent();
        }

        private void AcceptEvent()
        {
            MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.DeleteImageComplete, _imageInfo, null));
            CancelEvent();
        }

        private void CancelEvent() => SetActive(false);

        public void SetActive(bool state) => gameObject.SetActive(state);

        private void OnEnable()
        {
            _acceptButton.onClick.AddListener(AcceptEvent);
            _cancelButton.onClick.AddListener(CancelEvent);
        }

        private void OnDisable()
        {
            _acceptButton.onClick.RemoveListener(AcceptEvent);
            _cancelButton.onClick.RemoveListener(CancelEvent);
        }
    }
}