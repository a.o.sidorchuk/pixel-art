﻿using PixelArt.Data.Models;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.UI
{
    public class GalleryImageConainer : MonoBehaviour
    {
        [SerializeField] List<ImageContainerButton> _imageContainerButtons = new List<ImageContainerButton>();

        List<ImageInfo> _imageInfoItems = new List<ImageInfo>();

        public bool freeSlotCheck => _imageContainerButtons.Count > _imageInfoItems.Count;

        public void Init(List<ImageInfo> imageInfoItems)
        {
            _imageInfoItems = imageInfoItems;

            for (var i = 0; i < imageInfoItems.Count; i++) ButtonInit(i);

            if (freeSlotCheck) _imageContainerButtons.Last().gameObject.SetActive(false);
        }

        public void AddImageInfoToLastSlot(ImageInfo imageInfo)
        {
            _imageInfoItems.Add(imageInfo);
            _imageContainerButtons.Last().gameObject.SetActive(true);

            ButtonInit(_imageContainerButtons.Count - 1);
        }

        private void ButtonInit(int index)
        {
            var container = _imageContainerButtons[index];
            var imageInfo = _imageInfoItems[index];

            container.Init(imageInfo);
        }
    }
}