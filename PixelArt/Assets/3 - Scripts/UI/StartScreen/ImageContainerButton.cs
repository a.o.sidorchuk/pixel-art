﻿using Lean.Gui;
using PixelArt.Data.Models;
using PixelArt.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PixelArt.UI
{
    [Serializable]
    public class StatusIndicatorData
    {
        public ImageStatus imageStatus;
        public Color color;
        public Sprite sprite;
    }

    public class ImageContainerButton : LeanButton
    {
        [Header("Image Container Button Params")]
        [SerializeField] Image _buttonImage;
        [SerializeField] Image _statusIndicatorImage;
        [Space]
        [SerializeField] Image _progressImage;

        [Space]
        [SerializeField]
        List<StatusIndicatorData> _statusIndicators = new List<StatusIndicatorData>
        {
            new StatusIndicatorData { imageStatus = ImageStatus.None, color = Color.white },
            new StatusIndicatorData { imageStatus = ImageStatus.Completed, color = Color.green },
            new StatusIndicatorData { imageStatus = ImageStatus.Started, color = Color.yellow }
        };

        [Header("Remove Dialog")]
        [SerializeField] bool _dialogActive = true;
        [SerializeField] DeleteImageDialog _deleteImageDialog;
        bool _timerStarted = false;
        float _timer = 0;
        float _maxTimer = .8f;
        float _minTimer = -.4f;

        ImageInfo _imageInfo;

        public void Init(ImageInfo imageInfo)
        {
            _imageInfo = imageInfo;

            _deleteImageDialog.Init(_imageInfo);

            SetStatus(_imageInfo.imageStatus);

            if (_imageInfo.previewImage)
            {
                var prevImg = _imageInfo.previewImage;
                var rect = new Rect(0, 0, prevImg.width, prevImg.height);
                var pivot = new Vector2 { x = 0.5f, y = 0.5f };

                _buttonImage.preserveAspect = true;
                _buttonImage.sprite = Sprite.Create(_imageInfo.previewImage, rect, pivot);
            }

            OnClick.AddListener(ImageItemSelectedEvent);
        }

        private void SetStatus(ImageStatus status)
        {
            var indicator = _statusIndicators.FirstOrDefault(x => x.imageStatus == status);
            _statusIndicatorImage.color = indicator.color;
            _statusIndicatorImage.sprite = indicator.sprite;
        }

        private void ImageItemSelectedEvent()
        {
            SceneController.StartBySelectedImage(_imageInfo);
        }

        private void Update()
        {
            if (_timerStarted)
            {
                _timer = Mathf.Clamp(_timer + Time.deltaTime, _minTimer, _maxTimer);
                _progressImage.fillAmount = _timer / _maxTimer;
            }
            else
            {
                _progressImage.fillAmount = 0;
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            _timer = _minTimer;
            _timerStarted = true;

            base.OnPointerDown(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (_timer >= _maxTimer)
            {
                _deleteImageDialog.SetActive(true);

                DownPointers.Remove(eventData.pointerId);
            }
            else
            {
                base.OnPointerUp(eventData);
            }

            _timer = _minTimer;
            _timerStarted = false;
        }
    }
}