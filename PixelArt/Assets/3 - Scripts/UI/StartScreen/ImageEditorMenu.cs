﻿using PixelArt.Data;
using PixelArt.Data.Models;
using PixelArt.SceneManagement;
using PixelArt.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI
{
    public class ImageEditorMenu : MonoSingleton<ImageEditorMenu>
    {
        [SerializeField] CanvasGroup _canvasGroup;

        [Space]
        [SerializeField] RawImage _previewImage;

        [Space]
        [SerializeField] Slider _resizeSlider;
        [SerializeField] Text _resizeValueText;
        Array _resizeValues;
        string[] _resizeValueTexts;

        [Space]
        [SerializeField] Slider _paletteSlider;
        [SerializeField] Text _paletteValueText;
        Array _paletteSizeValues;
        string[] _paletteSizeValueTexts;

        [Space]
        [SerializeField] Button _applyButton;
        [SerializeField] Button _backButton;

        float _originalWidth;
        float _originalHeight;
        float _minZoom;
        float _maxZoom;
        Texture2D _originalImage;
        bool _portrait;


        Rect _cropRect;
        ResizeType _resizeType = ResizeType.Easy;
        PaletteSizeType _paletteSizeType = PaletteSizeType.Easy;


        private void Awake()
        {
            _originalImage = null;

            InitPreviewImage();

            InitSliderInfo();

            SetActive(false, 0);
        }

        public void SetActive(bool state, float fadeDuration = .5f) => UniTask.ToCoroutine(async () =>
        {
            if (state) Init();

            _canvasGroup.blocksRaycasts = state;
            _canvasGroup.interactable = state;

            await ScreenFader.FadeProcess(finalAlpha: state ? 1 : 0, fadeDuration, _canvasGroup);
        });

        private void Init()
        {
            _originalImage = AppStore.Instance.CurrentImageInfo.originalImage;

            _originalWidth = _originalImage.width;
            _originalHeight = _originalImage.height;

            _minZoom = Mathf.Min(_originalWidth, _originalHeight);
            _maxZoom = _minZoom * .1f;

            _portrait = _minZoom == _originalWidth;

            _cropRect = new Rect(0, 0, _minZoom, _minZoom);

            _previewImage.texture = _originalImage;

            UpdateUVRect();
        }
        
        private void ApplyButtonEvent()
        {
            var image = (Texture2D)_previewImage.texture;

            AppStore.Instance.CurrentImageInfo.SetImageData(image, _cropRect, _resizeType, _paletteSizeType);

            SceneController.StartGame();
        }

        private void BackButtonEvent()
        {
            AppStore.Instance.RemoveImageInfoFromProgress(AppStore.Instance.CurrentImageInfo);

            SetActive(false);
        }

        private void ZoomImage(float value, LayerMask layerMask)
        {
            if (value == 0 || (1 << _previewImage.gameObject.layer & layerMask) == 0) return;

            value *= Time.deltaTime * _minZoom * 5;
            var value2 = value * 2;

            var x = _cropRect.x;
            var y = _cropRect.y;
            var w = _cropRect.width;
            var h = _cropRect.height;

            if (value < 0)
            {
                if (w + value2 < _maxZoom) return;

                x -= value;
                y -= value;
                w += value2;
                h += value2;
            }
            else
            {
                x = Mathf.Clamp(x - value, 0, _originalWidth);
                y = Mathf.Clamp(y - value, 0, _originalWidth);

                _minZoom = _portrait ? _originalWidth - x : _originalHeight - y;

                w = Mathf.Clamp(w + value2, 0, _minZoom);
                h = Mathf.Clamp(h + value2, 0, _minZoom);
            }

            _cropRect.x = x;
            _cropRect.y = y;
            _cropRect.width = w;
            _cropRect.height = h;

            UpdateUVRect();
        }

        private void MoveImage(Vector3 deltaPos, Vector2 screenDelta, Vector3 worldPos, LayerMask layerMask)
        {
            if ((1 << _previewImage.gameObject.layer & layerMask) == 0) return;

            var value = deltaPos.normalized * -Time.deltaTime * _minZoom * Mathf.Max(_previewImage.uvRect.width, _previewImage.uvRect.height);

            _cropRect.x = Mathf.Clamp(_cropRect.x + value.x, 0, _originalWidth - _cropRect.width);
            _cropRect.y = Mathf.Clamp(_cropRect.y + value.y, 0, _originalHeight - _cropRect.height);

            UpdateUVRect();
        }

        private void PaletteTypeChanged(float value)
        {
            var index = (int)value;
            _paletteValueText.text = _paletteSizeValueTexts[index];
            _paletteSizeType = (PaletteSizeType)_paletteSizeValues.GetValue(index);
        }

        private void ResizeTypeChanged(float value)
        {
            var index = (int)value;
            _resizeValueText.text = _resizeValueTexts[index];
            _resizeType = (ResizeType)_resizeValues.GetValue(index);
        }
        
        private void UpdateUVRect()
        {
            _previewImage.uvRect = CalcViewRect();
        }

        private Rect CalcViewRect()
        {
            return new Rect
            {
                x = _cropRect.x / _originalWidth,
                y = _cropRect.y / _originalHeight,
                width = _cropRect.width / _originalWidth,
                height = _cropRect.height / _originalHeight
            };
        }

        private void OnEnable()
        {
            SceneManagement.PlayerInput.Instance.moveEvent += MoveImage;
            SceneManagement.PlayerInput.Instance.zoomEvent += ZoomImage;

            _applyButton.onClick.AddListener(ApplyButtonEvent);
            _backButton.onClick.AddListener(BackButtonEvent);

            _resizeSlider.onValueChanged.AddListener(ResizeTypeChanged);
            _paletteSlider.onValueChanged.AddListener(PaletteTypeChanged);
        }

        private void OnDisable()
        {
            SceneManagement.PlayerInput.Instance.moveEvent -= MoveImage;
            SceneManagement.PlayerInput.Instance.zoomEvent -= ZoomImage;

            _applyButton.onClick.RemoveListener(ApplyButtonEvent);
            _backButton.onClick.RemoveListener(BackButtonEvent);

            _resizeSlider.onValueChanged.RemoveListener(ResizeTypeChanged);
            _paletteSlider.onValueChanged.RemoveListener(PaletteTypeChanged);
        }

        private void InitPreviewImage()
        {
            _previewImage.texture = null;

            var parentTransform = _previewImage.rectTransform.parent.GetComponent<RectTransform>();
            var parentH = parentTransform.rect.height;
            var parentW = parentTransform.rect.width;

            var transform = _previewImage.rectTransform;

            if (parentTransform.rect.width > parentTransform.rect.height)
            {
                var offset = (parentW - parentH) / 2f;

                var min = transform.offsetMin;
                var max = transform.offsetMax;

                min.x = offset;
                max.x = -offset;

                transform.offsetMin = min;
                transform.offsetMax = max;            
            }
            else
            {
                var offset = (parentH - parentW) / 2f;

                var min = transform.offsetMin;
                var max = transform.offsetMax;

                min.y = offset;
                max.y = -offset;

                transform.offsetMin = min;
                transform.offsetMax = max;
            }
        }

        private void InitSliderInfo()
        {
            _resizeValues = Enum.GetValues(typeof(ResizeType));
            _resizeValueTexts = Enum.GetNames(typeof(ResizeType));
            _resizeSlider.maxValue = _resizeValues.Length - 1;
            _resizeSlider.value = Array.IndexOf(_resizeValues, _resizeType);
            ResizeTypeChanged(_resizeSlider.value);

            _paletteSizeValues = Enum.GetValues(typeof(PaletteSizeType));
            _paletteSizeValueTexts = Enum.GetNames(typeof(PaletteSizeType));
            _paletteSlider.maxValue = _paletteSizeValues.Length - 1;
            _paletteSlider.value = Array.IndexOf(_paletteSizeValues, _paletteSizeType);
            PaletteTypeChanged(_paletteSlider.value);
        }
    }
}