﻿using Lean.Gui;
using PixelArt.Data;
using PixelArt.Data.Models;
using PixelArt.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace PixelArt.UI
{
    public class StartMenu : MonoBehaviour
    {
        const int DEFAULT_IMAGE_COUNT = 8;
        const float DEFAULT_CONTAINER_HEIGHT = 520f;

        [SerializeField] LeanButton _randomImageBtn;
        [SerializeField] LeanButton _userImageBtn;
        Text _randomButtonText;
        string _defaultRandomButtonText;
        [Space]
        [SerializeField] RectTransform _galleryContentRoot;
        [SerializeField] GameObject _galleryImageContainerPrefab;

        int shownImageCount = 0;
        List<GalleryImageConainer> _containers = new List<GalleryImageConainer>();

        IDisposable _menuMsgSun;

        private void Awake()
        {
            _randomImageBtn.OnClick.AddListener(StartByRandomImage);
            _userImageBtn.OnClick.AddListener(StartByUserImage);

            _randomButtonText = _randomImageBtn.GetComponentInChildren<Text>();
            _defaultRandomButtonText = _randomButtonText.text;

            Init();

            _menuMsgSun = MessageBroker.Default.Receive<MenuMessage>()
                .Where(x => x.type == MenuMessage.Type.DeleteImageComplete)
                .Subscribe(DeleteImageCompleteEvent);
        }

        private void Init()
        {
            shownImageCount = 0;
            UpdateRandomButtonText();
            UpdateGalleryContent(DEFAULT_IMAGE_COUNT);
        }

        private void OnDestroy()
        {
            _menuMsgSun.Dispose();
        }

        private void DeleteImageCompleteEvent(MenuMessage msg) => UniTask.ToCoroutine(async () =>
        {
            var imageInfo = (ImageInfo)msg.data;

            await AppStore.DeleteImageFile(imageInfo);

            foreach (var c in _containers) Destroy(c.gameObject);
            _containers.Clear();

            Init();
        });


        private void StartByRandomImage()
        {
            _randomImageBtn.interactable = false;

            var startResult = SceneController.StartByRandomImage();

            _randomImageBtn.interactable = !startResult;
        }

        private void StartByUserImage()
        {
            _userImageBtn.interactable = false;

            UniTask.ToCoroutine(async () =>
            {
                await SceneController.StartByUserImage();

                _userImageBtn.interactable = true;
            });
        }

        private void UpdateGalleryContent(int imageCount) => UniTask.ToCoroutine(async () =>
        {
            var userImages = AppStore.Instance.UserProgress.imageItems.Skip(shownImageCount).Take(imageCount).ToList();

            if (userImages.Count == 0) return;

            shownImageCount += userImages.Count;

            // проверить свободный слот в последнем контейнере
            if (_containers.Count > 0 && _containers[_containers.Count - 1].freeSlotCheck)
            {
                var image = userImages[0];

                _containers[_containers.Count - 1].AddImageInfoToLastSlot(image);

                userImages = userImages.Skip(1).ToList();
            }

            if (userImages.Count == 0) return;

            // создать и заполнить новые контейнеры
            for (var i = 0; i < userImages.Count; i += 2)
            {
                var images = userImages.Skip(i).Take(2).ToList();

                await AppStore.LoadPreviewImages(images);

                var container = Instantiate(_galleryImageContainerPrefab, _galleryContentRoot)
                    .GetComponent<GalleryImageConainer>();

                container.Init(images);

                _containers.Add(container);
            }
        });


        public void ContentScrollEvent()
        {
            // _TODO проверить адекватность добавления новых картинок в галерею сверх того, что уже загружено
            // (картинок должно быть 10+) 

            var currentScrollValue = _galleryContentRoot.localPosition.y;
            var imageCount = Mathf.FloorToInt(currentScrollValue / DEFAULT_CONTAINER_HEIGHT) * 2 + DEFAULT_IMAGE_COUNT;

            if (imageCount > shownImageCount)
            {
                UpdateGalleryContent(imageCount - shownImageCount);
            }
        }

        private void UpdateRandomButtonText()
        {
            var notOpenImageCount = AppStore.Instance.GetCountNotOpenedImage();

            if (notOpenImageCount == 0) _randomImageBtn.interactable = false;

            _randomButtonText.text = _defaultRandomButtonText.Replace("#", $"{notOpenImageCount}");
        }
    }
}