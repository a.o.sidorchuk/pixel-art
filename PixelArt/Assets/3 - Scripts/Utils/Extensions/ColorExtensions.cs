﻿namespace UnityEngine
{
    public static class ColorExtensions
    {
        public static bool IsLight(this Color32 color)
        {
            return (0.299 * color.r + 0.587 * color.g + 0.114 * color.b) / 255 < 0.5;
        }
    }
}