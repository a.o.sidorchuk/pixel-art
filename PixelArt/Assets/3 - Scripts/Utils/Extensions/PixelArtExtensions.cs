﻿using PixelArt.Data.Models;
using PixelArt.Utils.Image;
using System.Linq;
using UnityEngine;

public static class PixelArtExtensions
{
    public static Texture2D ComplexProcessing(this Texture2D image, Rect cropArea, ResizeType resizeType, PaletteSizeType paletteSizeType)
    {
        image = image.CropAndResize(cropArea,(int)resizeType);
        return ColorImageQuantizer.ReduceColors(image, paletteSizeType);
    }

    public static ImageData ComplexProcessing(this Texture2D image, Rect cropArea, int newSize, int paletteSize)
    {
        image = image.CropAndResize(cropArea, newSize);
        return image.GenerateImageData(paletteSize);
    }

    public static Texture2D CropAndResize(this Texture2D image, Rect cropArea, int newSize)
    {
        // _TODO отпимизировать это сделав проход по краям картинки при сглаживании, без неободимости позже их удалять
        image = image.ApplyCrop(cropArea);
        image = image.ApplyResize(newSize + 2, newSize + 2);
        return image.ApplyCrop(new Rect(1, 1, newSize, newSize));
    }

    public static Texture2D ApplyCrop(this Texture2D image, Rect newRect)
    {
        return CropImageFilter.Apply(image, newRect);
    }

    public static Texture2D ApplyResize(this Texture2D image, int newWidth, int newHeight)
    {
        return ResizeImageFilter.Apply(image, newWidth, newHeight);
    }

    public static ImageData GenerateImageData(this Texture2D image, int paletteSize)
    {
        var palette = ColorImageQuantizer.CalculatePalette(image, paletteSize);

        return ColorImageQuantizer.ReduceColorsToData(image, palette);
    }

    public static Texture2D GeneratePreviewImage(this ImageData imageData)
    {
        var image = new Texture2D(imageData.width, imageData.height);
        var pixels = imageData.pixels.Select(x => x.GetColor()).ToArray();

        image.SetPixels32(pixels);

        return image;
    }
}