﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.ResourceProviders;
using Object = UnityEngine.Object;

namespace PixelArt.Utils
{
    public static class FileUtils
    {
        public const string PROGRESS_DATA_FILE_NAME = "user.data";
        public const string DEFAULT_IMAGE_URLS_FILE_NAME = "PixelArt/image-urls";
        const string META_FILE_EXT = ".meta";

        public static string GetFullFileName(string fileName) => $"{Application.persistentDataPath}/{fileName}";
        public static string GetMetaFileName(string fileName) => $"{GetFullFileName(fileName)}{META_FILE_EXT}";

        public static async UniTask<Texture2D> LoadImageFile(string fileName, bool remoteLoad = true)
        {
            var name = fileName.Split('/').Last();

            if (remoteLoad)
            {
                var image = await RemoteLoadAsset(fileName, new Texture2D(1, 1));
                image.name = name;
                return image;
            }
            else
            {
                if (!FileExistCheck(fileName)) return null;

                var data = await LoadFromDataFolder(fileName);

                if (data == null) return null;

                var image = new Texture2D(1, 1) { name = name };

                image.LoadImage(data);

                return image;
            }
        }

        public static async UniTask SaveImageFile(string fileName, Texture2D image)
        {
            var data = image.EncodeToPNG();
            await SaveToDataFolder(fileName, data);
        }


        public static async UniTask<T> LoadFromJson<T>(string fileName)
        {
            try
            {
                var bData = await LoadFromDataFolder(fileName);

                if (bData == null || bData.Length == 0) return default;

                var json = Encoding.Default.GetString(bData);

                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                Debug.Log(ex);

                return default;
            }
        }

        public static async UniTask SaveToJson<T>(string fileName, T data)
        {
            try
            {
                Debug.Log($"SaveToJson: {fileName}");

                var json = JsonConvert.SerializeObject(data);
                var bData = Encoding.Default.GetBytes(json);

                await SaveToDataFolder(fileName, bData);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }


        public static async UniTask SaveToDataFolder(string fileName, byte[] data)
        {
            using (var sw = new FileStream(fileName, FileMode.Create))
            {
                await sw.WriteAsync(data, 0, data.Length);
            }
        }

        public static async UniTask<byte[]> LoadFromDataFolder(string fileName)
        {
            using (var sw = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                var data = new byte[sw.Length];

                await sw.ReadAsync(data, 0, data.Length);

                return data;
            }
        }


        public static async UniTask<Texture2D> LoadImageFileFromWeb(string fileName)
        {
            var request = UnityWebRequestTexture.GetTexture(fileName);

            await request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                return null;
            }
            else
            {
                return ((DownloadHandlerTexture)request.downloadHandler).texture;
            }
        }

        public static async UniTask<T> RemoteLoadAsset<T>(string fileName, T resultContainer)
        {
            var completed = false;

            var req = Addressables.LoadAssetAsync<T>(fileName);
            req.Completed += (resource) =>
            {
                resultContainer = resource.Result;
                completed = true;
            };

            while (!completed) await UniTask.Yield();

            return resultContainer;
        }

        public static async UniTask<IList<T>> RemoteLoadAssets<T>(string lable, IList<T> resultContainer) where T : Object
        {
            var completed = false;

            var req = Addressables.DownloadDependenciesAsync(lable);
            req.Completed += (resource) =>
            {
                var bundleResource = (ICollection<IAssetBundleResource>)resource.Result;

                var bundle = bundleResource.FirstOrDefault();

                if (bundle != null)
                {
                    resultContainer = bundleResource.FirstOrDefault().GetAssetBundle().LoadAllAssets<T>();
                }

                completed = true;
            };

            while (!completed) await UniTask.Yield();

            return resultContainer;
        }

        public static async UniTask<string> LoadTextFromResourse(string fileName)
        {
            var textFile = (TextAsset)await Resources.LoadAsync<TextAsset>(fileName);
            return textFile.text;
        }

        public static async UniTask<List<string>> LoadDefaultAssetImageNames()
        {
            var result = new List<string>();
            var completed = false;

            Addressables.InitializeAsync().Completed += (resource) =>
            {
                var resources = resource.Result;

                var rgx = new Regex(@"(\W|^)_PixelArt(\W|$)");
                result = resources.Keys
                    .Select(x => x.ToString())
                    .Where(x => rgx.IsMatch(x))
                    .ToList();

                completed = true;
            };

            while (!completed) await UniTask.Yield();

            return result;
        }

        public static bool FileExistCheck(string fileName) => new FileInfo(fileName).Exists;

        public static void RemoveFile(string filePath)
        {
            if (!FileExistCheck(filePath)) return;
            File.Delete(filePath);
        }
    }
}