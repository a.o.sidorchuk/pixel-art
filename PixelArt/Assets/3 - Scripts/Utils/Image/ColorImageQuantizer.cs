﻿using PixelArt.Data.Models;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PixelArt.Utils.Image
{
    public enum ColorChanel { R, G, B }

    public class MedianCutCube
    {
        List<Color32> pixels;

        readonly byte minR, maxR;
        readonly byte minG, maxG;
        readonly byte minB, maxB;

        PaletteItem paletteItem = null;

        public int RedSize => maxR - minR;
        public int GreenSize => maxG - minG;
        public int BlueSize => maxB - minB;

        /// <summary>Элемент палитры на основе цвета и количесва пикселей в этом контейнере</summary>
        public PaletteItem PaletteItem
        {
            get
            {
                if (paletteItem == null)
                {
                    int red = 0, green = 0, blue = 0;

                    foreach (var color in pixels)
                    {
                        red += color.r;
                        green += color.g;
                        blue += color.b;
                    }

                    int pixelCount = pixels.Count;

                    if (pixelCount != 0)
                    {
                        red /= pixelCount;
                        green /= pixelCount;
                        blue /= pixelCount;
                    }

                    var colorResult = new Color32((byte)red, (byte)green, (byte)blue, 255);

                    paletteItem = new PaletteItem
                    {
                        color = colorResult,
                        isWhiteColor = colorResult.IsWhiteColor()
                    };
                }

                return paletteItem;
            }
        }

        public MedianCutCube(List<Color32> pixels)
        {
            this.pixels = pixels;

            minR = minG = minB = 255;
            maxR = maxG = maxB = 0;

            foreach (var color in pixels)
            {
                if (color.r < minR) minR = color.r;
                if (color.r > maxR) maxR = color.r;

                if (color.g < minG) minG = color.g;
                if (color.g > maxG) maxG = color.g;

                if (color.b < minB) minB = color.b;
                if (color.b > maxB) maxB = color.b;
            }
        }

        /// <summary>Разделить контейнер на два меньших согласно выбранному цветовому каналу</summary>
        public void SplitAtMedian(ColorChanel colorType, out MedianCutCube cube1, out MedianCutCube cube2)
        {
            switch (colorType)
            {
                case ColorChanel.R: pixels.Sort(new RedComparer()); break;
                case ColorChanel.G: pixels.Sort(new GreenComparer()); break;
                case ColorChanel.B: pixels.Sort(new BlueComparer()); break;
            }

            var median = pixels.Count / 2;

            cube1 = new MedianCutCube(pixels.GetRange(0, median));
            cube2 = new MedianCutCube(pixels.GetRange(median, pixels.Count - median));
        }

        #region sorting color helpers
        private class RedComparer : IComparer<Color32>
        {
            public int Compare(Color32 c1, Color32 c2) => c1.r.CompareTo(c2.r);
        }
        private class GreenComparer : IComparer<Color32>
        {
            public int Compare(Color32 c1, Color32 c2) => c1.g.CompareTo(c2.g);
        }
        private class BlueComparer : IComparer<Color32>
        {
            public int Compare(Color32 c1, Color32 c2) => c1.b.CompareTo(c2.b);
        }
        #endregion
    }

    public static class MedianCutQuantizer
    {
        /// <summary>Получить палитру указанного размера</summary>
        public static List<PaletteItem> GetPalette(Color32[] pixels, int paletteSize)
        {
            var cubes = new List<MedianCutCube> { new MedianCutCube(new List<Color32>(pixels)) };

            SplitCubes(cubes, paletteSize);

            var palette = FilteringRepeatPaletteItems(cubes.Select(x => x.PaletteItem).ToList()).ToList();

            return palette;
        }

        private static List<PaletteItem> FilteringRepeatPaletteItems(List<PaletteItem> palette)
        {
            bool checkColor(PaletteItem x, PaletteItem item)
            {
                var xc = x.color;
                var ic = item.color;

                var checkR = xc.r < ic.r + 10 && xc.r > ic.r - 10;
                var checkG = xc.g < ic.g + 10 && xc.g > ic.g - 10;
                var checkB = xc.b < ic.b + 10 && xc.b > ic.b - 10;

                return checkR && checkG && checkB;
            }

            var result = new List<PaletteItem>();

            foreach (var item in palette)
            {
                var resItem = result.FirstOrDefault(x => checkColor(x, item));

                if (resItem == null)
                    result.Add(item);
                else
                    resItem.isWhiteColor = item.isWhiteColor;
            }

            return result;
        }

        /// <summary>Разделяем контенеры, пока не получим желаемый размер палитры</summary>
        private static void SplitCubes(List<MedianCutCube> cubes, int count)
        {
            var cubeIndexToSplit = cubes.Count - 1;

            while (cubes.Count < count)
            {
                MedianCutCube cube1, cube2;
                var cubeToSplit = cubes[cubeIndexToSplit];

                // определяем цветовой канал с наибольшим "весом"
                if ((cubeToSplit.RedSize >= cubeToSplit.GreenSize) && (cubeToSplit.RedSize >= cubeToSplit.BlueSize))
                {
                    cubeToSplit.SplitAtMedian(ColorChanel.R, out cube1, out cube2);
                }
                else if (cubeToSplit.GreenSize >= cubeToSplit.BlueSize)
                {
                    cubeToSplit.SplitAtMedian(ColorChanel.G, out cube1, out cube2);
                }
                else
                {
                    cubeToSplit.SplitAtMedian(ColorChanel.B, out cube1, out cube2);
                }

                cubes.RemoveAt(cubeIndexToSplit);
                cubes.Insert(cubeIndexToSplit, cube1);
                cubes.Insert(cubeIndexToSplit, cube2);

                if (--cubeIndexToSplit < 0) cubeIndexToSplit = cubes.Count - 1;
            }
        }
    }

    public static class ColorImageQuantizer
    {
        /// <summary>Сгенерировать данные изображения с меньшим количеством цветов</summary>
        public static ImageData ReduceColors(Texture2D image, int paletteSize)
        {
            return ReduceColorsToData(image, CalculatePalette(image, paletteSize));
        }

        /// <summary>Сгенерировать изображение с меньшим количеством цветов</summary>
        public static Texture2D ReduceColors(Texture2D image, PaletteSizeType paletteSizeType)
        {
            return ReduceColorsToTexture(image, CalculatePalette(image, (int)paletteSizeType));
        }

        /// <summary>Сгенерировать данные изображения с меньшим количеством цветов используя указанную палитру</summary>
        public static ImageData ReduceColorsToData(Texture2D image, List<PaletteItem> palette)
        {
            paletteToUse = palette;
            cache.Clear();

            var width = image.width;
            var height = image.height;
            var imagePixels = image.GetPixels32();

            var pixels = new List<PixelItem>(imagePixels.Length);

            for (int y = 0, i = 0, h = height; y < h; y++)
            {
                for (int x = 0, w = width; x < w; x++, i++)
                {
                    var paletteItem = GetPletteItem(imagePixels[i]);
                    var position = new Vector3Int { x = x, y = y };

                    paletteItem.pixelCount++;

                    pixels.Add(new PixelItem(paletteItem, position));
                }
            }

            // формирование индекса для элемента палитры
            palette.ForEach(x => x.isWhiteColor = x.isWhiteColor || x.pixelCount == 0);
            palette = palette.OrderBy(x => x.isWhiteColor).ToList();
            for (var i = 0; i < palette.Count; i++) palette[i].id = i;

            var pixelCount = palette.Where(x => !x.isWhiteColor).Sum(x => x.pixelCount);
            return new ImageData(width, height, palette, pixels, pixelCount);
        }


        /// <summary>Сгенерировать изображение с меньшим количеством цветов используя указанную палитру</summary>
        public static Texture2D ReduceColorsToTexture(Texture2D image, List<PaletteItem> palette)
        {
            paletteToUse = palette;
            cache.Clear();

            var width = image.width;
            var height = image.height;
            var imagePixels = image.GetPixels32();
            var resultPixels = new Color32[imagePixels.Length];

            for (int y = 0, i = 0, h = height; y < h; y++)
            {
                for (int x = 0, w = width; x < w; x++, i++)
                {
                    resultPixels[i] = GetPletteItem(imagePixels[i]).color;
                }
            }

            var resultImage = new Texture2D(width, height, image.format, false)
            {
                filterMode = FilterMode.Point,
                anisoLevel = 0
            };

            resultImage.SetPixels32(resultPixels);

            return resultImage;
        }

        /// <summary>Вычислить новую палитру для указанного изображения</summary>
        public static List<PaletteItem> CalculatePalette(Texture2D image, int paletteSize)
        {
            var colors = image.GetPixels32();

            return MedianCutQuantizer.GetPalette(colors, paletteSize);
        }

        #region Helper methods
        static List<PaletteItem> paletteToUse;
        static Dictionary<Color, PaletteItem> cache = new Dictionary<Color, PaletteItem>();

        /// <summary>Получить ближайший цвет из палитры</summary>
        private static PaletteItem GetPletteItem(Color32 color)
        {
            if (cache.ContainsKey(color)) return cache[color];

            var colorIndex = 0;
            var minError = int.MaxValue;

            for (int i = 0, n = paletteToUse.Count; i < n; i++)
            {
                var dr = color.r - paletteToUse[i].color.r;
                var dg = color.g - paletteToUse[i].color.g;
                var db = color.b - paletteToUse[i].color.b;

                var error = dr * dr + dg * dg + db * db;

                if (error < minError)
                {
                    minError = error;
                    colorIndex = i;
                }
            }

            var paletteItem = paletteToUse[colorIndex];

            cache.Add(color, paletteItem);

            return paletteItem;
        }
        #endregion
    }

    public static class Color32Extensions
    {
        public static bool IsWhiteColor(this Color32 color) => color.r > 240 && color.g > 240 && color.b > 240;
    }
}