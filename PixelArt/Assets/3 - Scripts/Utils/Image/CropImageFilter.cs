﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PixelArt.Utils.Image
{
    public static class CropImageFilter
    {
        public static Texture2D Apply(Texture2D image, Rect rect)
        {
            var newImage = new Texture2D((int)rect.width, (int)rect.height, image.format, false);

            rect.x = Mathf.FloorToInt(rect.x);
            rect.y = Mathf.FloorToInt(rect.y);
            rect.width = newImage.width;
            rect.height = newImage.height;

            ProcessFilter(image, newImage, rect);

            return newImage;
        }

        private static void ProcessFilter(Texture2D source, Texture2D destination, Rect rect)
        {
            var xMin = (int)rect.xMin;
            var xMax = (int)rect.xMax;
            var yMin = (int)rect.yMin;
            var yMax = (int)rect.yMax;

            var srcWidth = source.width;

            var srcPixels = source.GetPixels32();
            var dstPixels = new Color32[destination.width * destination.height];

            for (int y = yMin, i = 0; y < yMax; y++)
            {
                for (int x = xMin; x < xMax; x++, i++)
                {
                    dstPixels[i] = srcPixels[ImageUtils.CalcIndex(x, y, srcWidth)];
                }
            }

            destination.SetPixels32(dstPixels);
        }
    }
}

