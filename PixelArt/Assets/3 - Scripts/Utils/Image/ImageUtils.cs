﻿namespace PixelArt.Utils.Image
{
    public class ImageUtils
    {
        public static int CalcIndex(int x, int y, int width) => y * width + x;
    }
}