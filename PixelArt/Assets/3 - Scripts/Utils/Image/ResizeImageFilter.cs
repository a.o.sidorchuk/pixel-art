﻿using UnityEngine;

namespace PixelArt.Utils.Image
{
    public static class ResizeImageFilter
    {
        public static Texture2D Apply(Texture2D image, int newWidth, int newHeight)
        {
            var newImage = new Texture2D(newWidth, newHeight, image.format, false);

            ProcessFilter(image, newImage);

            return newImage;
        }

        private static void ProcessFilter(Texture2D source, Texture2D destination)
        {
            var width = source.width;
            var height = source.height;
            int newWidth = destination.width;
            int newHeight = destination.height;

            var xFactor = Mathf.FloorToInt(width / newWidth);
            var yFactor = Mathf.FloorToInt(height / newHeight);

            var srcPixels = source.GetPixels32();
            var dstPixels = new Color32[newWidth * newHeight];

            for (int y = 0, i = 0; y < newHeight; y++)
            {
                var yy = y * yFactor;

                for (var x = 0; x < newWidth; x++, i++)
                {
                    var ii = ImageUtils.CalcIndex(x * xFactor, yy, width);
                    dstPixels[i] = srcPixels[ii];
                }
            }

            // _TODO поиграть с коэфицентами или убрать сглаживание

            // или как вариант дополнить алгоритм проходом по крайним точкам, 
            // без необходимости обрезать их после

            // применение сглаживания сверточной матрицей 
            var dstPixels2 = new Color32[newWidth * newHeight];
            //var bMatrix = new[] 
            //{ 
            //    .5f, .75f, .5f, 
            //    .75f, 1f, .75f, 
            //    .5f, .75f, .5f 
            //};
            //var coef = 6f;
            var bMatrix = new[]
            {
                .25f, .5f, .25f,
                .5f,   1,  .5f,
                .25f, .5f, .25f
            };
            var coef = 4f;
            var smoothing = true;

            for (int y = 1, h = newHeight - 1; y < h; y++)
            {
                var matrixCell = CalcMatrixArray(1, y, newWidth);
                var currentIndex = ImageUtils.CalcIndex(1, y, newWidth);

                for (int x = 1, w = newWidth - 1; x < w; x++)
                {
                    float r = 0, g = 0, b = 0;

                    for (int i = 0; i < 9; i++)
                    {
                        var color = dstPixels[matrixCell[i]];
                        var bFactor = bMatrix[i];

                        r += color.r * bFactor;
                        g += color.g * bFactor;
                        b += color.b * bFactor;
                    }

                    dstPixels2[currentIndex] = new Color32
                    {
                        r = (byte)Mathf.Max(0, Mathf.Min(255, (r / coef))),
                        g = (byte)Mathf.Max(0, Mathf.Min(255, (g / coef))),
                        b = (byte)Mathf.Max(0, Mathf.Min(255, (b / coef))),
                        a = 255
                    };

                    currentIndex++;
                    matrixCell = ShiftMatrixArray(matrixCell);
                }
            }

            if (smoothing)
                destination.SetPixels32(dstPixels2);
            else
                destination.SetPixels32(dstPixels);
        }

        private static int[] ShiftMatrixArray(int[] matrix)
        {
            for (var i = 0; i < matrix.Length; i++) matrix[i]++;
            return matrix;
        }

        private static int[] CalcMatrixArray(int x, int y, int width)
        {
            var i1 = ImageUtils.CalcIndex(x - 1, y - 1, width);
            var i2 = i1 + width;
            var i3 = i2 + width;

            return new[]
            {
                i1, i1 + 1, i1 + 2,
                i2, i2 + 1, i2 + 2,
                i3, i3 + 1, i3 + 2
            };
        }
    }
}