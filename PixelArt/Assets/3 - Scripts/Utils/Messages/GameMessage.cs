﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UniRx
{
    public class GameMessage : UniRxMessage<GameMessage, GameMessage.Type, MonoBehaviour, object>
    {
        public enum Type
        {
            PickPaletteItem,
            PickBrushItem,
            ResetPickBrushItem,
            AutozoomStart,
            AutozoomComplete,
            EndGame,
            RestartGame,
            BrushReloadIncrement
        }
    }
}