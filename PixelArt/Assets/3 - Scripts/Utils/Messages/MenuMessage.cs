﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UniRx
{
    public class MenuMessage : UniRxMessage<MenuMessage, MenuMessage.Type, MonoBehaviour, object>
    {
        public enum Type
        {
            DeleteImageComplete
        }
    }
}