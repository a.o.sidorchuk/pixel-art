﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniRx
{
    public class UniRxMessage<T, MessageType, Sender, Data> where T : UniRxMessage<T, MessageType, Sender, Data>, new()
    {
        public MessageType type { get; protected set; }
        public Sender sender { get; protected set; }
        public Data data { get; protected set; }

        public static T Create(MessageType type, Data data, Sender sender)
        {
            return new T
            {
                type = type,
                sender = sender,
                data = data
            };
        }
    }
}
