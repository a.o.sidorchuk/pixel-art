﻿using UnityEngine;

namespace PixelArt.Utils
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        [SerializeField] bool isDontDestroyObject = false;

        static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance) return _instance;

                _instance = FindObjectOfType<T>();

                if (_instance) return _instance;

                Create();

                return _instance;
            }
        }

        void Awake() => CustomAwake();

        protected virtual void CustomAwake() => DontDestroy();

        private static void Create()
        {
            var typeName = typeof(T).Name;

            var singletonPrefab = Resources.Load<T>(typeName);

            if (singletonPrefab)
                _instance = Instantiate(singletonPrefab);
            else
                _instance = new GameObject(typeName).AddComponent<T>();
        }

        private void DontDestroy()
        {
            if (!isDontDestroyObject) return;

            if (this == Instance)
            {
                if (transform.parent) transform.parent = null;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }
    }
}

